import json
import os
import numpy as np
import subprocess
import yaml
import boto3
from botocore.exceptions import ClientError


def read_yaml_file(path):
    with open(path, "r") as f:
        data = yaml.safe_load(f)
    return data

def write_yaml_file(path, data):
    with open(path, "w") as f:
        yaml.dump(data, f)

def create_folder_on_another_computer(path:str):
    if os.path.exists(path):
        return
    try:
        os.mkdir(path)
    except OSError:
        subprocess.call('net use {} /user:{} {}'.format(path, "www", 'Atin12345'), shell=True)
        create_folder_on_another_computer(path)
        
def create_folder(path:str):
    if os.path.exists(path):
        return
    os.mkdir(path)

def read_json_data(fp):
    assert os.path.isfile(fp), "{} is not a file".format(fp)
    with open(fp, mode='r', encoding='utf-8') as f:
        dt = json.load(f)
        return dt

def write_json_data(fp, json_data:str):
    with open(fp, mode='w', encoding='utf8') as f:
        json.dump(json_data, f, ensure_ascii=False, indent=4)
        
def convert_2_point_to_4_point(polygon):
    x1, y1, x2, y2 = polygon[0]
    points = np.array([[x1, y1], [x1, y2], [x2, y2], [x2, y1]], dtype=np.int32)
    points = points.reshape((-1, 1, 2))
    return points

def upload_file_to_cloud(s3_client, filename, bucket = "pt-traffic", object_name=None):
    """Upload a file to an S3 bucket

    :param file_name: File to upload
    :param bucket: Bucket to upload to
    :param object_name: S3 object name. If not specified then file_name is used
    :return: True if file was uploaded, else False
    """

    # If S3 object_name was not specified, use file_name
    if object_name is None:
        object_name = os.path.basename(filename)

    # Upload the file
    try:
        response = s3_client.upload_file(filename, bucket, object_name)
    except ClientError as e:
        return False
    return True

import cv2

def write_image_to_cloud(s3_client, image, file_path, bucket = "pt-traffic", object_name = None):
    param = [cv2.IMWRITE_JPEG_QUALITY, 85]

    image_string = cv2.imencode('.jpg', image, param)[1].tostring()
    try:
        response = s3_client.put_object(Body=image_string, Key=file_path, Bucket=bucket)
    except ClientError as e:
        print("Error at write image to cloud, error: ", e)
        return False
    return True

     



