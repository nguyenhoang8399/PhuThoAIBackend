from PyQt5 import QtCore, QtWidgets, QtGui
import cv2
from ...template import Ui_DrawPolygon
from ...model import Camera
import os

class WgDrawPolygonController(QtWidgets.QWidget):
    def __init__(self):
        super().__init__()
        self.setWindowFlags(QtCore.Qt.WindowType.WindowStaysOnTopHint)
        self.setMouseTracking(True)
        self.ui = Ui_DrawPolygon()
        self.ui.setupUi(self)
        self._camera = None
        self._polygon = None
        self._frame = None
        self._old_polygon = None

    @property
    def camera(self):
        return self._camera

    @camera.setter
    def camera(self, camera: Camera):
        self._camera = camera
        
    @property
    def polygon(self):
        return self._polygon

    @polygon.setter
    def polygon(self, polygon):
        self._polygon = polygon
        
    def slot_setup(self):
        source = self._camera.link
        cap = cv2.VideoCapture(source)
        if cap.isOpened():
            self._frame = cap.read()[1]
        else:
            print("Can not read camera!")
            return
        w, h = self.ui.qlabel_frame.width(), self.ui.qlabel_frame.height()
        
        # create Label
        self.ui.qlabel_frame = Label(self, self._polygon)

        def _handle_new_polygon_event(x):
            self._polygon[:] = x

        self.ui.qlabel_frame.sig_polygon_save.connect(_handle_new_polygon_event)

        self.ui.gridLayout.addWidget(self.ui.qlabel_frame, 0, 1, 1, 1)

        self.slot_get_frame(self._frame)
        self.ui.qlabel_frame._w = w
        self.ui.qlabel_frame._h = h
        self.ui.qlabel_frame._load_polygon()
        self._connect_signal_button()

    def _connect_signal_button(self):
        self.ui.btn_save.clicked.connect(
            self.ui.qlabel_frame.slot_save_polygon)
        self.ui.btn_clear_all.clicked.connect(
            self.ui.qlabel_frame.slot_clear_all)

    def slot_get_frame(self, frame):
        self.ui.qlabel_frame._frame = frame


class Label(QtWidgets.QLabel):
    sig_polygon_save = QtCore.pyqtSignal(list)

    def __init__(self, parent, old_polygon):
        super().__init__(parent)
        
        self._frame = None
        self._set_variable_for_mouse()
        self._old_polygon = old_polygon
        self._w, self._h = 0, 0

    def slot_save_polygon(self):
        list_point = self._bbox_qlabel_to_frame(self._list_point)
        if list_point:
            self.sig_polygon_save.emit(list_point)
            print(f"{self.__class__.__name__} new polygon: ", list_point)
            self.parent().close()

    def slot_clear_all(self):
        self._list_point = []

    def _set_variable_for_mouse(self):
        self._x1 = 0
        self._y1 = 0
        self._x2 = 0
        self._y2 = 0
        self._is_pressed = False

    def _delete_rectangle(self, point: tuple):
        x, y = point
        for x1, y1, x2, y2 in self._list_point[:]:
            if x1 < x < x2 and y1 < y < y2:
                self._list_point.remove([x1, y1, x2, y2])

    def _load_polygon(self):
        self._list_point = self._old_polygon
        self._list_point = sorted(self._list_point, key=lambda x: x[0])
        print("Load polygon before: ", self._list_point)
        self._list_point = self._bbox_frame_to_qlabel(self._list_point)
        print("Load polygon: ", self._list_point)

    def _bbox_frame_to_qlabel(self, bboxes):
        new_bbox = []
        H, W = self._frame.shape[:2]
        print(self._w, self._h)
        for bbox in bboxes:
            x1, y1, x2, y2 = bbox
            x1 = int(x1 / W * self._w)
            y1 = int(y1 / H * self._h)
            x2 = int(x2 / W * self._w)
            y2 = int(y2 / H * self._h)
            new_bbox.append([x1, y1, x2, y2])
        return new_bbox

    def _bbox_qlabel_to_frame(self, bboxes):
        new_bbox = []
        self._w, self._h = self.width(), self.height()
        print(self._w, self._h)
        H, W = self._frame.shape[:2]
        for bbox in bboxes:
            x1, y1, x2, y2 = bbox
            x1 = int(x1 / self._w * W)
            y1 = int(y1 / self._h * H)
            x2 = int(x2 / self._w * W)
            y2 = int(y2 / self._h * H)
            new_bbox.append([x1, y1, x2, y2])
        return new_bbox

    def mouseMoveEvent(self, event):
        if self._is_pressed:
            self._x2 = event.pos().x()
            self._y2 = event.pos().y()

    def mousePressEvent(self, event):
        if event.button() == QtCore.Qt.LeftButton:
            self._is_pressed = True
            self._x1 = event.pos().x()
            self._y1 = event.pos().y()
            self._x2 = -1000

        if event.button() == QtCore.Qt.RightButton:
            x = event.pos().x()
            y = event.pos().y()
            self._delete_rectangle((x, y))

    def mouseReleaseEvent(self, event):
        if event.button() == QtCore.Qt.LeftButton:
            self._is_pressed = False
            self._x2 = event.pos().x()
            self._y2 = event.pos().y()
            if self._x2 - self._x1 < 20:
                self._x2 = -1000
                return
            self._list_point.append([self._x1, self._y1, self._x2, self._y2])
            self._list_point = sorted(self._list_point, key=lambda x: x[0])
            self._x2 = -1000

    def paintEvent(self, event):
        painter = QtGui.QPainter(self)
        if self._frame is not None:
            current_frame = self._frame
            self._show_frame(painter, current_frame)
        painter.setPen(QtGui.QPen(QtCore.Qt.red, 2, QtCore.Qt.SolidLine))
        for idx, bbox in enumerate(self._list_point):
            x1, y1, x2, y2 = bbox
            w, h = x2 - x1, y2 - y1
            rect = QtCore.QRect(x1, y1, w, h)
            painter.drawRect(rect)
            painter.drawText(x1, y1 - 10, str(idx + 1))
        if self._x2 > 0:
            rect = QtCore.QRect(self._x1, self._y1, self._x2 -
                                self._x1, self._y2 - self._y1)
            painter.drawRect(rect)
        self.update()

    def _show_frame(self, painter, current_frame):
        if current_frame is not None:
            rgb_img = cv2.cvtColor(current_frame, cv2.COLOR_BGR2RGB)
            qt_img = QtGui.QPixmap.fromImage(
                QtGui.QImage(rgb_img.data, rgb_img.shape[1], rgb_img.shape[0], QtGui.QImage.Format_RGB888)).scaled(
                self.width(), self.height())
            painter.drawPixmap(self.rect(), qt_img)
