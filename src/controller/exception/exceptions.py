
class ConnectionError:
    CANNOT_CONNECT_TO_WEB = "Không thể kết nối đến hệ thống web"
    CANNOT_CONNECT_TO_FRS = "Không thể kết nối đến hệ thống nhận diện khuôn mặt"
    CANNOT_CONNECT_TO_POLYGON_SERVER = "Không thể kết nối đến hệ thống thông tin vùng giám sát"


class ValueException:
    CAMERA_FIELD_ERROR = "Thông tin camera không hợp lệ"
    POLYGON_FIELD_ERROR = "Thông tin vùng giám sát không hợp lệ"
    

class ConnectToWebException:
    CANNOT_FETCH_DATA_FROM_WEB = "Không thể lấy thông tin camera từ hệ thống web"
    CANNOT_GET_POLYGON_DATA = "Không thể lấy thông tin vùng giám sát từ hệ thống web"
    