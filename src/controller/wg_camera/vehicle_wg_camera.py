from ..thread import (
                        DigitAndPlateColorRecognitionThread, 
                        GeneralThread, 
                        PlateThread,
                        RecordThread, 
                        VehicleTrackingThread, 
                        CaptureThread,
                        VehicleApiThread,
                        StreamingThread
)
from ...model import Camera, VehicleResult
from ...service import VehicleApi
from .base_camera import BaseWgCamera
import cv2
from threading import Thread
import asyncio
from src.util import write_image_to_cloud, get_logger
from src.config import ROOT_SAVE_FILE_VEHICLE, ROOT,  HOST_S3, S3_BUCKET_NAME, S3_CLIENT


class VehicleWgCamera(BaseWgCamera):
    def __init__(self, camera:Camera):
        super().__init__(camera)
        self._list_thread  = []
        self.veh_api = VehicleApi()
        self._vehicle_in_black_list = []
        self.__create_thread()
        self.__connect_signal()
        
    @property
    def vehicle_in_blacklist(self):
        return self._vehicle_in_black_list
 
    @vehicle_in_blacklist.setter
    def vehicle_in_blacklist(self, value):
        self._vehicle_in_black_list[:] = value
 
    def __create_thread(self):
        self.__capture_thread = CaptureThread(self, self._camera)
        
        self.__record_thread = RecordThread(self, self._camera)
        
        
        # --- AI threads ---
        self.__track_thread = VehicleTrackingThread(self, 
                                                    self._camera,
                                                    in_buffer = self.__capture_thread.out_buffer)
        
        self.__record_thread.buffer = self.__track_thread.out_buffer_record # ref to record buffer of share_mem_thread
        
        self.__plate_thread = PlateThread(self,
                                          camera = self._camera, 
                                          in_buffer = self.__track_thread.plate_buffer)
        
        self.__digit_and_plate_recognition_thread = DigitAndPlateColorRecognitionThread(self, 
                                                                                        camera=self._camera,
                                                                                        in_buffer = self.__plate_thread.digit_buffer)
        
        self.__general_thread = GeneralThread(self,
                                              camera = self._camera, 
                                              in_digit_buffer = self.__digit_and_plate_recognition_thread.out_digit_buffer)
        
        self.__general_thread.vehicle_in_blacklist = self._vehicle_in_black_list
        
        self._paint_event_buffer = self.__track_thread.paint_event_buffer # reference to output buffer of general thread for showing
        
         # --- END AI threads ---
        
        self.__streaming_thread = StreamingThread(self, self._camera, self.__track_thread.streaming_buffer)
        
        self.__veh_api_thread = VehicleApiThread(self)
        
        # References
        self.__digit_and_plate_recognition_thread.tracked_vehicle_object_manager = self.__plate_thread.tracked_vehicle_object_manager
        self.__plate_thread.tracked_id_pushed_to_web_server = self.__general_thread.tracked_id_pushed_to_web_server
        
        self._list_thread = [self.__capture_thread, 
                             self.__record_thread,
                             self.__track_thread,
                            #  self.__streaming_thread,
                             self.__plate_thread, 
                             self.__digit_and_plate_recognition_thread,
                             self.__general_thread,
                             self.__veh_api_thread] # append for managing
        
    def __connect_signal(self):
        self.__general_thread.sig_veh_info.connect(self.__veh_api_thread.append_to_list_vehicle_result)
        self.__general_thread.sig_save_image.connect(self.__save_img)
        self.__general_thread.sig_blacklist.connect(self.__record_thread.append_to_event_list)
    
    
    def __save_img(self, image_info):
        fp, image = image_info
        
        # img_encode = cv2.imencode('.jpg', image, param)[1]
       
        # new_thread = Thread(target=lambda: cv2.imwrite(fp, image), daemon=True)
        # new_thread.start()
        write_image_to_cloud(S3_CLIENT, image, fp)

    def start_streaming_thread(self):
        if not self.__streaming_thread.is_active:
            self.__streaming_thread.start()

    def stop_streaming_thread(self):
        if self.__streaming_thread.is_active:
            self.__streaming_thread.stop()