from PyQt5.QtCore import QObject
from ...model import Camera
from .person_wg_camera import PersonWgCamera
from .vehicle_wg_camera import VehicleWgCamera
from .base_camera import BaseWgCamera
from typing import List


class ListWgsController(QObject):
    def __init__(self, list_wgs):
        super().__init__()
        self.__list_wgs:List[BaseWgCamera] = list_wgs
        
    @property
    def list_wgs(self):
        return self.__list_wgs
    
    
    def find_wgs_by_camera(self, camera:Camera):
        if self.__list_wgs:
            for e in self.__list_wgs:
                if e.camera == camera:
                    return e
        return None
    
    def find_indexs_of_wg_by_function(self, camera_function = 1):
        list_index = []
        if self.__list_wgs:
            for k, v in enumerate(self.__list_wgs):
                if v.camera.function == camera_function:
                    list_index.append(k)
        return list_index
    
    def start_wg(self, camera:Camera):
        wg = self.find_wgs_by_camera(camera)
        print("-----------------------------start wg camera id: ", camera.id)
        assert wg is not None, "Widget of camera with id {camera.id} is not initted" 
        wg.start_thread()    
    
    def stop_wg(self, camera:Camera):
        wg = self.find_wgs_by_camera(camera)
        assert wg is not None, "Widget of camera with id {camera.id} is not initted" 
        wg.stop_thread()
    
    def start_all_wgs(self):
        if self.__list_wgs:
            for e in self.__list_wgs:
                e.start_thread()
                
    def stop_all_wgs(self):
        if self.__list_wgs:
            for e in self.__list_wgs:
                e.stop_thread()
                
    def remove_wgs(self, camera:Camera):
        wgs = self.find_wgs_by_camera(camera)
        self.__list_wgs.remove(wgs)
        
    @staticmethod
    def create_wgs(camera:Camera):
        if camera.function == 1: #person camera
            new_wg = PersonWgCamera(camera)
        if camera.function == 2:
            new_wg = VehicleWgCamera(camera)  
        return new_wg
    
            