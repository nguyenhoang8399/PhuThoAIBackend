from ..thread import (FaceTrackingThread,
                      CaptureThread, 
                      FaceRecognitionThread, 
                      RecordThread, 
                      StreamingThread, 
                      PersonApiThread)
from ...model import Camera
from .base_camera import BaseWgCamera
import cv2
from threading import Thread
from multiprocessing import Process

class PersonWgCamera(BaseWgCamera):
    def __init__(self, camera:Camera):
        super().__init__(camera)
        self.__create_thread()
        self.__connect_signal()
    
    def __create_thread(self):
        self.__capture_thread = CaptureThread(self, self._camera)

        #create and setup Tracking
        self.__tracking_thread = FaceTrackingThread(self,self._camera,  self.__capture_thread.out_buffer)
        self.__tracking_thread.camera = self._camera
        self._paint_event_buffer = self.__tracking_thread.paint_event_buffer # reference process_buffer for paint event in qlabel
        
        self.__face_recog_thread = FaceRecognitionThread(self, self._camera, self.__tracking_thread.out_buffer)
        # self.__face_recog_thread.person_object_manager = self.__tracking_thread.person_object_manager
        self.__record_thread = RecordThread(self, self._camera)
        self.__record_thread.buffer = self.__tracking_thread.out_buffer_record
        self.__person_api_thread = PersonApiThread(self)
        self.__thread_stream = StreamingThread(self, self._camera, self.__tracking_thread.stream_buffer)
        self._list_thread = [self.__capture_thread,
                             self.__tracking_thread,
                             self.__thread_stream,
                             self.__face_recog_thread,
                             self.__record_thread,
                             self.__person_api_thread] # append for managing
    
        
    def __connect_signal(self):
        # self.__tracking_thread.signal_person_object.connect(self.__face_recog_thread.append_to_tracked_person_buffer)
        self.__face_recog_thread.signal_black_list.connect(self.__record_thread.append_to_event_list)
        self.__face_recog_thread.signal_save_image.connect(self.__save_img)
        self.__face_recog_thread.signal_person_info.connect(self.__person_api_thread.append_to_list_person_result)
        
        # if tracking id is posted to face server or web server, append to lists of tracking thread
        # for handle color bbox of the object
        self.__face_recog_thread.signal_tracked_id_posted_to_frs.connect(self.__tracking_thread.append_tracked_id_posted_to_frs)
        self.__person_api_thread.signal_is_posted_to_web_server.connect(self.__tracking_thread.append_tracked_id_posted_to_web_server) 

    def __save_img(self, image_info):
        fp, image = image_info
        new_thread = Thread(target=lambda: cv2.imwrite(fp, image), daemon=True)
        new_thread.start()
        
        

