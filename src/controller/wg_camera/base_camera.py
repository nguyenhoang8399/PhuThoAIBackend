from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from ...template import Ui_WgCamera
from ...model import Camera
from ..thread import *
from queue import Queue
import numpy as np
import cv2
import time
from ..wg_draw_polygon import WgDrawPolygonController

class BaseWgCamera(QWidget):
    signal_camera = pyqtSignal(Camera, str)
    
    def __init__(self, camera: Camera):
        super().__init__()
        self.ui_element = Ui_WgCamera()
        self.ui_element.setupUi(self)
        self.camera = camera
        self.installEventFilter(self)
        self._paint_event_buffer: Queue = None
        self._list_thread: list
        self.__last_time = time.time()
        
        
        self.__wg_draw_polygon = WgDrawPolygonController()
        self.__wg_draw_polygon.camera = self._camera
        

    @property
    def camera(self):
        return self._camera

    @camera.setter
    def camera(self, camera: Camera):
        self._camera = camera

    def eventFilter(self, obj, event) -> bool:
        if event.type() == QMouseEvent.Type.MouseButtonDblClick:
            if event.button() == Qt.MouseButton.LeftButton:
                self._handle_left_click_event()
                return True
            if event.button() == Qt.MouseButton.RightButton:
                self._handle_right_click_event()
                return True
        else:
            return False
        return super(BaseWgCamera, self).eventFilter(obj, event)

    def _handle_right_click_event(self):
        self.signal_camera.emit(self._camera, "right_click_event")
        print("right click event")

    def _handle_left_click_event(self):
        self.signal_camera.emit(self._camera, "left_click_event")
        print("left click event")

    # def paintEvent(self, a0: QPaintEvent) -> None:
    #     if self._paint_event_buffer.qsize() > 0:
    #         image = self._paint_event_buffer.get()
    #         # cv2.putText(image, f"FPS: {1 / (time.time() - self.__last_time):.2f}", (100, 100), cv2.FONT_HERSHEY_SIMPLEX, 2, (0, 255, 0), 2)
    #         cv2.putText(image, "Camera ID: " + str(self._camera.id), (100, image.shape[0] - 10), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 2)
    #         self.__last_time = time.time()
    #         self._update_images(image)
    #     self.update()
    #     time.sleep(0.001)

    def _update_images(self, images):
        if images is not None:
            # rgb_img = cv2.resize(images, (self.ui_element.qlabel_frame.width(), self.ui_element.qlabel_frame.height()))
            rgb_img = cv2.cvtColor(images, cv2.COLOR_BGR2RGB)
            qt_img = QPixmap.fromImage(
                QImage(
                    rgb_img.data, rgb_img.shape[1], rgb_img.shape[0], QImage.Format_RGB888)
            ).scaled(self.ui_element.qlabel_frame.width(), self.ui_element.qlabel_frame.height())
            self.ui_element.qlabel_frame.setPixmap(qt_img)
        # self.update()

    def reset_wg(self):
        if self._paint_event_buffer.qsize() != 0:
            self._paint_event_buffer.queue.clear()
        frame = self.ui_element.qlabel_frame
        black_background = np.zeros((frame.width(), frame.height()))
        qt_img = QPixmap.fromImage(
            QImage(black_background.data,
                   black_background.shape[1], black_background.shape[0], QImage.Format_RGB888)
        ).scaled(frame.width(), frame.height())
        self.ui_element.qlabel_frame.setPixmap(qt_img)

    def start_thread(self):
        for e in self._list_thread:
            e.start()

    def stop_thread(self):
        for e in self._list_thread:
            e.stop()
