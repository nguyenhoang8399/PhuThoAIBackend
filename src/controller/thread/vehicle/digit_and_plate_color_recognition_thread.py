from PyQt5 import QtCore
from PyQt5.QtCore import QObject, QThread, QUuid
from ....yolov5.detect_bt import Detection, Classify
from ....yolov5 import read_model_config_file
from queue import Queue
from .tracked_vehicle_object import TrackedVehicle
from .tracked_vehicle_object_manager import TrackedVehicleManager
from ....util import get_logger
from ....config import ROOT, DEBUG_MODE
import os
from ....model import Camera
import numpy as np
from typing import List
import cv2


SAVE_DATA_PLATE = os.path.join(ROOT, "data", "07022923/plate")
SAVE_DATA_VEHICLE = os.path.join(ROOT, "data", "07022923/vehicle")


class DigitAndPlateColorRecognitionThread(QThread):
    def __init__(self, parent,camera:Camera, in_buffer):
        super().__init__(parent)
        self.__thread_active = False
        self.__camera = camera
        self.__in_buffer: Queue = in_buffer
        self.__out_digit_buffer = Queue()
        self.__max_size_out_buffer = 100
        self.__digit_detector = Detection()
        self.__lpcol_classifier = Classify()
        self.__tracked_vehicle_object_manager: TrackedVehicleManager
        self.tracked_id_pushed_to_web_server = None
        self._logger = get_logger(file_name=os.path.join(ROOT, "log", f"{self.__camera.id}digit_plate_color_recognition_thread.log"),
                                  name_logger="digit_plate_color_recognition_thread")
        
        
    @property
    def out_digit_buffer(self):
        return self.__out_digit_buffer

    @property
    def tracked_vehicle_object_manager(self):
        return self.__tracked_vehicle_object_manager

    @tracked_vehicle_object_manager.setter
    def tracked_vehicle_object_manager(self, value: TrackedVehicleManager):
        self.__tracked_vehicle_object_manager = value
        self.__tracked_vehicle_object_manager.sig_missed_tracked_vehicle.connect(
            self.add_missing_tracked_vehicle_to_out_buffer)
        
 

    def add_missing_tracked_vehicle_to_out_buffer(self, tracked_vehicle: TrackedVehicle):
        if self.__out_digit_buffer.qsize() < self.__max_size_out_buffer:
            self.__out_digit_buffer.put(tracked_vehicle)
            # print("add_missing_tracked_vehicle_to_out_buffer ", tracked_vehicle.plate)

            if DEBUG_MODE:
                self._logger.info("*** add_missing_tracked_vehicle_to_out_buffer  {}".format(tracked_vehicle.deserialize()))
                self._logger.info("len of list frame contain plate number: {}".format(len(tracked_vehicle.list_frame_contain_plate_number)))
                self._logger.info("is in plate zone: {}".format(tracked_vehicle.is_in_plate_zone))
                self._logger.info("is missing: {}".format(tracked_vehicle.is_missing))
            tracked_vehicle.is_pushed_to_general_buffer = True

    def __setup_digit(self):
        a = "digit"
        weights, classes, conf, imgsz, device, data = read_model_config_file(a)
        self.__digit_detector.setup_model(
            weights, classes, conf, imgsz, device,  data)

    def __setup_lpcol(self):
        a = "lpcol"
        weights, classes, conf, imgsz, device, data = read_model_config_file(a)
        self.__lpcol_classifier.setup_model(
            weights, classes, conf, imgsz, device)

    def __is_square_plate(self, dg_crop):
        return dg_crop.shape[1] / dg_crop.shape[0] <= 2.8

    def __process_square_plate(self, dg_dict):
        l_1 = []
        l_2 = []
        all_y = [box[1] for box in dg_dict]
        if len(all_y) == 0:
            return []
        average_y = sum(all_y) / len(all_y)
        for bbox in dg_dict:
            if bbox[1] < average_y:
                l_1.append(bbox)
            else:
                l_2.append(bbox)
        l_1 = sorted(l_1, key=lambda x: x[0])
        l_2 = sorted(l_2, key=lambda x: x[0])
        return l_1 + l_2

    def __process_plate(self, dg_dict):
        plate = ''
        for i in range(len(dg_dict)):
            x1, y1, x2, y2, cls, conf = dg_dict[i]
            cls = str(cls)
            cx, cy = (x1 + x2) / 2, (y1 + y2) / 2
            digit = cls.upper()
            for j in range(len(dg_dict)):
                if j == i:
                    continue
                x11, y11, x22, y22, clss, conff = dg_dict[j]
                if x11 < cx < x22 and y11 < cy < y22:
                    if float(conf) > float(conff):
                        digit = cls.upper()
            plate += digit
        return plate

    @staticmethod
    def most_frequent(ls):
        if ls:
            try:
                return max(set(ls), key=ls.count)
            except Exception as er:
                print(er)
                return ''
        return ''

    @staticmethod
    def check_condition_of_plate(plate_value:str):
        import re
        if re.search('[A-Z]', plate_value):
            if plate_value[0].isnumeric(): #eg 9H12345 is not valid, 99H12352 is valid
                if plate_value[1].isnumeric():
                    part_value = plate_value[3:]
                    if len(part_value) > 3:
                        return True
                    else: 
                        return False
                else:
                    return False
            return True
        return False
        

    def run(self):
        self.__setup_digit()
        # self.__setup_lpcol()
        self.__thread_active = True
        while self.__thread_active:
            if self.__in_buffer.qsize() > 0:
                # contains box of vehicle object and its plate (from plate thread)
                frame, object_tracked_dict, plate_dict = self.__in_buffer.get()
                
                for tracked_id, plate_box in plate_dict.items():
                    tracked_vehicle = self.__tracked_vehicle_object_manager.find_vehicle_by_tracked_id(tracked_id)
                    
                    if tracked_vehicle is None:
                        continue

                    plate_box = plate_dict[tracked_id]
                    bbox_vehicle = object_tracked_dict[tracked_id]
                    x1_, y1_, x2_, y2_, cls = bbox_vehicle

                    vehicle_crop = frame[y1_:y2_, x1_:x2_]
                    x1, y1, x2, y2 = plate_box[:4]
                    w_dg, h_dg = x2 - x1, y2 - y1
                    if w_dg < 30:
                        continue
                    
                    dg_crop = vehicle_crop[y1:y2, x1:x2]
                    processed_plate_number = self.__digit_detector.preprocess_img(dg_crop)
                    detected_dg = self.__digit_detector.detect(processed_plate_number, dg_crop)
                    detected_dg_with_name = [[x1, y1, x2, y2, self.__digit_detector.names[int(cls)], conf] for x1, y1, x2, y2, cls, conf in detected_dg]
                    dg_dict = sorted(detected_dg_with_name, key=lambda x: x[0])

                    if self.__is_square_plate(dg_crop):
                        dg_dict = self.__process_square_plate(dg_dict)
                    plate = self.__process_plate(dg_dict)
                                        
                    if int(cls) == 1:
                        if len(plate) < 7:  # handling digit for motobike
                            continue
                        else:
                            if not self.check_condition_of_plate(plate):
                                continue 
                            tracked_vehicle.list_plate_number.append(plate)
                            mean_conf_digit_detection = np.mean([conf for _, _, _, _, _, conf in detected_dg])
                            tracked_vehicle.list_plate_crop.append((dg_crop, plate, mean_conf_digit_detection))
                            tracked_vehicle.list_frame_contain_plate_number.append((frame, bbox_vehicle[:4], plate))
                    else:
                        if len(plate) < 6:  # handling digit for car
                            continue
                        else:
                            if not self.check_condition_of_plate(plate):
                                continue 
                            tracked_vehicle.list_plate_number.append(plate)
                            mean_conf_digit_detection = np.mean([conf for _, _, _, _, _, conf in detected_dg])
                            tracked_vehicle.list_plate_crop.append((dg_crop, plate, mean_conf_digit_detection))
                            tracked_vehicle.list_frame_contain_plate_number.append((frame, bbox_vehicle[:4], plate))                   

                    tracked_vehicle.plate = TrackedVehicle.most_frequent(tracked_vehicle.list_plate_number)
                    
                    if (not tracked_vehicle.is_in_plate_zone) or (len(tracked_vehicle.list_frame_contain_plate_number) > 15):
                        if DEBUG_MODE:
                            self._logger.info("***   {}".format(tracked_vehicle.deserialize()))
                            self._logger.info("len of list frame contain plate number: {}".format(len(tracked_vehicle.list_frame_contain_plate_number)))
                            self._logger.info("is in plate zone: {}".format(tracked_vehicle.is_in_plate_zone))
                            self._logger.info("is missing: {}".format(tracked_vehicle.is_missing))
                            self._logger.info("len of list cls: {}".format(len(tracked_vehicle.list_cls)))
                            
                        # cv2.imwrite(SAVE_DATA_PLATE + f'/{QUuid.createUuid().toString(QUuid.StringFormat.WithoutBraces)}.jpg', dg_crop)
                        # cv2.imwrite(SAVE_DATA_VEHICLE + f'/{QUuid.createUuid().toString(QUuid.StringFormat.WithoutBraces)}.jpg', vehicle_crop)
                        
                        if not tracked_vehicle.is_pushed_to_general_buffer:
                            if self.__out_digit_buffer.qsize() < self.__max_size_out_buffer:
                                self.__out_digit_buffer.put(tracked_vehicle)
                                tracked_vehicle.is_pushed_to_general_buffer = True
            self.msleep(1)
    def stop(self):
        self.__thread_active = False
