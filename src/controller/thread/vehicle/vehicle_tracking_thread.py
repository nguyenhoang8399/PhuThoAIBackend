from PyQt5.QtCore import QThread, QDateTime
from ....yolov5 import Tracking, VehicleTracking
from ....yolov5.setup import read_model_config_file
from queue import Queue
from time import sleep
from ....model import Camera, Image
import cv2
import time
from collections import deque


class VehicleTrackingThread(QThread):
    MAX_SIZE_RECORD_BUFFER = 200  # for n frames
    
    def __init__(self, parent, camera:Camera, in_buffer):
        super().__init__(parent)
        self.__camera = camera
        self.__thread_active = False
        self.__in_buffer:Queue = in_buffer
        
        self.__plate_buffer = Queue()
        self.__tracking_buffer = Queue()
        self.__out_buffer_record = deque(maxlen=self.MAX_SIZE_RECORD_BUFFER)        
        self.__streaming_buffer = Queue()
        self.__paint_event_buffer = Queue()
        self.__max_size_out_buffer = 100
        self.__tracking = VehicleTracking()

    @property
    def tracking_buffer(self):
        return self.__tracking_buffer
    
    @property
    def plate_buffer(self):
        return self.__plate_buffer
    
    @property
    def streaming_buffer(self):
        return self.__streaming_buffer
    
    @property
    def paint_event_buffer(self):
        return self.__paint_event_buffer
    
    @property
    def out_buffer_record(self):
        return self.__out_buffer_record
    
    def __setup_tracking(self):
        weights, classes, conf, imgsz, device, data = read_model_config_file("track")
        self.__tracking.setup_model(weights, classes, conf, imgsz, device, data)

    def __put_to_queue(self, q:Queue, thing_to_put):
        if q.qsize() < self.__max_size_out_buffer:
            q.put(thing_to_put)

    def run(self):
        self.__setup_tracking()
        self.__thread_active = True
        # calculate fps
        count = 0
        fps = 0
        old_time = time.time()
        
        while self.__thread_active:
            if time.time() - old_time > 1:
                fps = count
                count = 0
                old_time = time.time()
                
            if self.__in_buffer.qsize() > 0:
                frame = self.__in_buffer.get()
                # copied_frame = frame.copy()
                processed_frame = self.__tracking.preprocess_img(frame)
                id_dict = self.__tracking.track(processed_frame, frame) # current object ids and its bbox
                cv2.putText(frame, f"FPS: {fps}", (100, 100), cv2.FONT_HERSHEY_SIMPLEX, 2, (0, 255, 0), 2)
                count += 1
                # self.__put_to_queue(self.__tracking_buffer, (frame, id_dict))
                self.__put_to_queue(self.__plate_buffer, (frame, id_dict))
                
                self.__out_buffer_record.append(Image(frame, QDateTime.currentDateTime()))

                if self.__streaming_buffer.qsize() < 3:
                    self.__streaming_buffer.put((frame, id_dict))
                    
                # if self.__paint_event_buffer.qsize() < 3:
                #     self.__paint_event_buffer.put((frame, id_dict))
            sleep(0.001)
            
    def stop(self):
        self.__thread_active = False
        if len(self.__out_buffer_record):
            self.__out_buffer_record.clear()

