from datetime import datetime
from PyQt5.QtCore import QUuid, QThread, pyqtSignal
import cv2
import time
from ....yolov5.plate_casting import *
from ....yolov5 import read_model_config_file
from ....yolov5.detect_bt import Classify
import os
from ....config import ROOT_SAVE_FILE_VEHICLE, ROOT,  HOST_S3, S3_BUCKET_NAME, S3_CLIENT
from ....model import Camera, Polygon, VehicleResult
from queue import Queue
from ....util import write_image_to_cloud, get_logger
import numpy as np
from collections import deque
from .tracked_vehicle_object import TrackedVehicle
from .tracked_vehicle_object_manager import TrackedVehicleManager
from typing import List
import platform

class GeneralThread(QThread):
    sig_veh_info = pyqtSignal(object)
    sig_save_image = pyqtSignal(tuple)
    sig_blacklist = pyqtSignal(VehicleResult)

    def __init__(self, parent, camera: Camera, in_digit_buffer: Queue):
        super().__init__(parent)
        self.__thread_active = False
        self.__in_digit_buffer = in_digit_buffer
        self.__camera = camera
        self.tracked_id_pushed_to_web_server = deque(maxlen=1000)
        self.__logger = get_logger(file_name=os.path.join(
            ROOT, "log", "general_thread.log"), name_logger="general_thread")
        self.__lpcol_classifier = Classify()
        self.__server_name = platform.node()
        

    @property
    def vehicle_in_blacklist(self):
        return self.__list_vehicle_in_blacklist
    
    @vehicle_in_blacklist.setter
    def vehicle_in_blacklist(self, data:List):
        self.__list_vehicle_in_blacklist = data

    def __setup_lpcol(self):
        weights, classes, conf, imgsz, device, data = read_model_config_file("lpcol")
        self.__lpcol_classifier.setup_model(weights, classes, conf, imgsz, device)

    @staticmethod
    def get_letter_path(prefix):
        return os.path.abspath(prefix)[0] # Windows only

    def run(self):
        self.__thread_active = True
        # self.__setup_lpcol()
        
        while self.__thread_active:
            if self.__in_digit_buffer.qsize() > 0:
                tracked_vehicle: TrackedVehicle = self.__in_digit_buffer.get()
                tracked_id = tracked_vehicle.tracked_id
                if tracked_id in self.tracked_id_pushed_to_web_server:
                    continue
                tracked_vehicle.plate = TrackedVehicle.most_frequent(tracked_vehicle.list_plate_number)
                plate_crop = tracked_vehicle.get_best_plate_for_color_classify(tracked_vehicle.plate)
                # rs_color = self.__lpcol_classifier.classify(plate_crop)
                # tracked_vehicle.color = rs_color[0]
                tracked_vehicle.cls = TrackedVehicle.most_frequent(tracked_vehicle.list_cls)
                
                digit, lp_color, cls = tracked_vehicle.plate, tracked_vehicle.color, tracked_vehicle.cls
                
                if digit:
                    # create result
                    # print("digit: ", digit)
                    rs = VehicleResult()
                    rs.digit = digit
                    # rs.type = check_plate(digit, lp_color, cls)
                    rs.cls = int(cls)
                    
                    rs.camera_id = self.__camera.id
                    rs.area_id = self.__camera.area_id
                    date_folder = datetime.now().strftime('%d_%m_%Y')
                    
                    img_fp = os.path.join("images/" + date_folder)
                    video_fp = os.path.join("videos/" + date_folder)
                    
                    rs.img_path = f'{HOST_S3}/{S3_BUCKET_NAME}/images/{date_folder}/{rs.event_id}.jpg'
                    rs.plate_img_path = f'{HOST_S3}/{S3_BUCKET_NAME}/plates/{date_folder}/{rs.event_id}.jpg'
                    rs.video_path = f"{HOST_S3}/{S3_BUCKET_NAME}/videos/{date_folder}/{rs.event_id}.mp4"
    
                    frame_cp, location = tracked_vehicle.get_best_frame(digit)
                    
                    flp = f"plates/{date_folder}/" + rs.event_id + ".jpg"
                    write_image_to_cloud(S3_CLIENT, plate_crop, flp)
                    
                    fp = f"images/{date_folder}/" + rs.event_id + ".jpg"
                    x1_, y1_, x2_, y2_ = location
                    cv2.rectangle(frame_cp, (x1_, y1_),
                                  (x2_, y2_), (0, 0, 255), 1)

                    # send to wgs for create thread to save image
                    self.sig_save_image.emit((fp, frame_cp))
                    
                    # send to wg for create thread to send data to web server
                    self.sig_veh_info.emit(rs)
                    
                    tracked_vehicle.is_posted_to_web_server = True
                    self.tracked_id_pushed_to_web_server.append(tracked_id)
            time.sleep(0.001)

    def stop(self):
        self.__thread_active = False
