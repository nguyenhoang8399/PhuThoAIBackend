from PyQt5 import QtCore
from PyQt5.QtCore import QTimer, QDateTime, pyqtSignal
from ....yolov5.detect_bt import Detection
from ....yolov5 import read_model_config_file
from ....model import Polygon, Camera
from queue import Queue
import cv2
from .tracked_vehicle_object import TrackedVehicle
from .tracked_vehicle_object_manager import TrackedVehicleManager
import time
from ....yolov5 import relu
from collections import deque
import numpy as np


class PlateThread(QtCore.QThread):
    def __init__(self, parent, camera:Camera, in_buffer:Queue):
        super().__init__(parent)
        self.__thread_active = False
        self.__camera = camera
        self.__in_buffer = in_buffer
        self.__out_digit_buffer = Queue()
        self.__max_size_out_buffer = 100
        self.__plate_detection = Detection()
        self.__tracked_vehicle_object_manager = TrackedVehicleManager()
        self.tracked_id_pushed_to_web_server:deque
 
    @property
    def digit_buffer(self):
        return self.__out_digit_buffer

    @property
    def tracked_vehicle_object_manager(self):
        return self.__tracked_vehicle_object_manager

    def setup_plate(self):
        weights, classes, conf, imgsz, device, data = read_model_config_file("plate")
        self.__plate_detection.setup_model(weights, classes, conf, imgsz, device, data)

    @staticmethod
    def is_in_polygon(polygon, box, cls:int):
        x1, y1, x2, y2, _ = box
        if cls == 1:
            return cv2.pointPolygonTest(polygon, ((x1 + x2) // 2, (y1 + y2) // 2), False) > 0 # detect center of box is in polygon if motorbike
        else: 
            return cv2.pointPolygonTest(polygon, ((x1 + x2) // 2, y2), False) > 0 # detect center of bottom edge is in polygon if car

    def __put_to_queue(self, q:Queue, value):
        if q.qsize() < self.__max_size_out_buffer:
            q.put(value)

    @staticmethod
    def is_plate_in_2_bbox(box_plate, id_dict):
        count = 0
        for _, bbox in id_dict.items():
            x1, y1, x2, y2 = bbox[:4]
            center_plate_x, center_plate_y = (box_plate[0] + box_plate[2]) // 2, (box_plate[3] + box_plate[1]) // 2
            is_in_box = x1 <= center_plate_x <= x2 and y1 <= center_plate_y <= y2
            if is_in_box:
                count += 1
        return count > 1

    def run(self):
        self.setup_plate()
        self.__thread_active = True
        while self.__thread_active:
            
            if self.__in_buffer.qsize() > 0:
                frame, object_tracked_dict = self.__in_buffer.get()
                
                plate = {}
                H, W = frame.shape[:2]
                list_points_format = Polygon.convert_to_point_format(self.__camera.polygon.area_active, W, H)
                
                for tracked_id, bbox in object_tracked_dict.items():
                    if tracked_id in self.tracked_id_pushed_to_web_server:
                        continue
                    
                    tracked_vehicle = self.__tracked_vehicle_object_manager.find_vehicle_by_tracked_id(tracked_id)
                    box = bbox[:5]
                    x1, y1, x2, y2, cls = box

                    if not self.is_in_polygon(list_points_format, box, int(cls)):
                        if tracked_vehicle is None:
                            continue
                        
                    vehicle_crop = frame[y1:y2, x1:x2]
                    
                    if self.is_in_polygon(list_points_format, box, int(cls)):
                        preprocess_vehicle_crop = self.__plate_detection.preprocess_img(vehicle_crop)
                        plate_list = self.__plate_detection.detect(preprocess_vehicle_crop, vehicle_crop)
                        if tracked_vehicle is None:
                            tracked_vehicle = TrackedVehicle(tracked_id)
                            self.__tracked_vehicle_object_manager.update(tracked_vehicle)
                        tracked_vehicle.list_cls.append(cls)
                        tracked_vehicle.last_time_in_plate_zone = QDateTime.currentSecsSinceEpoch()
                        
                        if len(plate_list) > 1:
                            for e in plate_list:
                                plate_box_e = list(map(lambda x: int(relu(x)), e[:4]))
                                cp_x, cp_y = (plate_box_e[0] + plate_box_e[2]) // 2, (plate_box_e[3] + plate_box_e[1]) // 2
                                h_vehicle, w_vehicle = vehicle_crop.shape[:2]

                                if self.is_plate_in_2_bbox(e, object_tracked_dict) or not (h_vehicle // 2 <= cp_y <= h_vehicle):
                                    print("box", h_vehicle // 2, cp_y, h_vehicle)

                                    plate_list.remove(e)
                                
                                # if not (h_vehicle // 2 <= cp_y <= h_vehicle):
                                #     plate_list.remove(e)

                                # condition = (x1 <= cp_x <= x2) and (((y1 + y2) // 2) <= cp_y <= y2)
                                # if not condition:
                                #     plate_list.remove(e)
                        
                        if plate_list:
                            plate_box = plate_list[0]
                            plate_box = list(map(lambda x: int(relu(x)), plate_box[:4]))
                            # center_plate_x, center_plate_y = (plate_box[0] + plate_box[2]) // 2, (plate_box[3] + plate_box[1]) // 2
                            # h_vehicle, w_vehicle = vehicle_crop.shape[:2]
                            # if h_vehicle // 2 <= center_plate_y <= h_vehicle:
                            #     plate[tracked_id] = plate_box
                            # else:
                            #     print("box", h_vehicle // 2, center_plate_y, h_vehicle)
                            plate[tracked_id] = plate_box


                            # plate[tracked_id] = list(map(lambda x: int(relu(x)), plate_box[:4]))
                    else:
                        # tracked_vehicle = self.__tracked_vehicle_object_manager.find_vehicle_by_tracked_id(tracked_id)
                        if tracked_vehicle is not None:
                            tracked_vehicle.is_in_plate_zone = False
                self.__put_to_queue(self.__out_digit_buffer, (frame, object_tracked_dict, plate))
            time.sleep(0.001)

    def stop(self):
        self.__thread_active = False
