from collections import deque
from PyQt5.QtCore import QDateTime
import numpy as np

class TrackedVehicle:
    def __init__(self, tracked_id):
        self.tracked_id = tracked_id
        self.cls = 0
        self.plate = ""
        self.color = ""
        self.vehicle_type = "" 
        self.type = "" # car or motobike
        
        self.list_frame_contain_plate_number = deque(maxlen=30) # [(frame, bbox, plate_number), ...]
        self.last_time_in_plate_zone = QDateTime.currentSecsSinceEpoch()
        
        self.list_cls = deque(maxlen=30)
        self.list_plate_number = deque(maxlen=30)
        self.list_plate_crop = deque(maxlen=30)
        self.list_color = deque(maxlen=30)
        
        
        self.is_missing = False
        self.is_in_plate_zone = True
        self.is_posted_to_web_server = False
        self.is_pushed_to_general_buffer = False
  
    def get_best_frame(self, plate_number) -> tuple:
        if self.list_frame_contain_plate_number:
            list_frame_contain_plate_number = list(filter(lambda x: x[2] == plate_number, self.list_frame_contain_plate_number))
            max_size_obj = 0
            list_size = [(e[1][2] - e[1][0]) * (e[1][3] - e[1][1]) for e in list_frame_contain_plate_number]
            max_size_obj = max(list_size)
            
            for e in list_frame_contain_plate_number:
                img, bbox = e[0], e[1]
                w, h = bbox[2] - bbox[0], bbox[3] - bbox[1]
                if w * h == max_size_obj:
                    return (img.copy(), bbox)     
        return None
    
    def get_best_plate_for_color_classify(self, plate_number):
        if self.list_plate_crop:
            list_plate_crop_contain_number = list(filter(lambda x: x[1] == plate_number, self.list_plate_crop))
            list_plate_crop_contain_number = sorted(list_plate_crop_contain_number, key=lambda x: x[2], reverse=True)
            return list_plate_crop_contain_number[0][0] # [(plate_crop, plate_number, score) ...]
        return None

    def deserialize(self):
        dict_ = {
            "tracked_id": self.tracked_id,
            "cls": self.cls,
            "plate": self.plate,
            "color": self.color,
            "vehicle_type": self.vehicle_type
        }
        return dict_
    
    @staticmethod
    def most_frequent(ls):
        if ls:
            try:
                return max(set(ls), key=ls.count)
            except Exception as er:
                print(er)
                return ''
        return ''
    
    
    
    
    