from PyQt5.QtCore import QTimer, QDateTime, pyqtSignal, QObject
from typing import List
from .tracked_vehicle_object import TrackedVehicle
from typing import Dict
from ....util import get_logger
from ....config import ROOT, DEBUG_MODE
import os

class TrackedVehicleManager(QObject):
    sig_missed_tracked_vehicle = pyqtSignal(TrackedVehicle) # connect to digit_plate_and_color_recognition_thread
    
    def __init__(self):
        super().__init__()
        self.dict_tracked_vehicle:Dict[int, TrackedVehicle] = {}
        self.max_allowed_alive_time = 1 # in seconds
        
        self.timer_check_alive_time_to_remove = QTimer()
        self.timer_check_alive_time_to_remove.timeout.connect(self.__clean_cache_and_emit_missed_tracked_object)
        self.timer_check_alive_time_to_remove.setInterval(100)
        self.timer_check_alive_time_to_remove.start()
        self.__logger = get_logger(os.path.join(ROOT, "log", "tracked_vehicle_manager.log"), name_logger="tracked_vehicle_manager")
        
    def update(self, tracked_vehicle:TrackedVehicle):
        tracked_id = tracked_vehicle.tracked_id
        if tracked_id not in self.dict_tracked_vehicle:
            self.dict_tracked_vehicle[tracked_id] = None
        self.dict_tracked_vehicle[tracked_id] = tracked_vehicle
    
    def find_vehicle_by_tracked_id(self, tracked_id):
        if tracked_id in self.dict_tracked_vehicle:
            return self.dict_tracked_vehicle[tracked_id]
        return None
    
    def check_condition_to_post_to_web_server(self, tracked_vehicle:TrackedVehicle):
        is_missing = QDateTime.currentSecsSinceEpoch() - tracked_vehicle.last_time_in_plate_zone > self.max_allowed_alive_time
        return (len(tracked_vehicle.plate) > 0) and (not tracked_vehicle.is_posted_to_web_server) and is_missing
    
    def __force_del_tracked_object(self):
        for k in list(self.dict_tracked_vehicle.keys()):
            tracked_vehicle = self.dict_tracked_vehicle[k]
            last_time_in_plate_zone = tracked_vehicle.last_time_in_plate_zone
            tracked_id = tracked_vehicle.tracked_id
            max_allowed_time_in_cache = 10
            if QDateTime.currentSecsSinceEpoch() - last_time_in_plate_zone > max_allowed_time_in_cache:
                del self.dict_tracked_vehicle[tracked_id]
                if DEBUG_MODE:
                    self.__logger.info("Remove tracked vehicle with id: {}".format(tracked_id))
    
    def __clean_cache_and_emit_missed_tracked_object(self):
        # number_of_posted_to_web_server = 0
        for k in list(self.dict_tracked_vehicle.keys()):
            tracked_object = self.dict_tracked_vehicle[k]
            condition = self.check_condition_to_post_to_web_server(tracked_object)
            
            if condition:
                # print("Missed tracked vehicle: {}".format(tracked_object.plate))
                self.sig_missed_tracked_vehicle.emit(tracked_object)
            else:
                if tracked_object.is_posted_to_web_server:
                    del self.dict_tracked_vehicle[k]
                    if DEBUG_MODE:
                        self.__logger.info("Remove tracked vehicle with id: {}".format(k))
                        
        self.__force_del_tracked_object()
            
    