import ffmpeg
import numpy
from PyQt5 import QtCore
from PyQt5.QtCore import pyqtSignal
import time
import cv2
from ....model import PersonResult, Camera, VehicleResult, BaseResult
from collections import deque
from ....config import ROOT_SAVE_FILE_PERSON, ROOT, DEBUG_MODE, ROOT_SAVE_FILE_VEHICLE
import os
from ....util import get_logger
from ....model import Image
from typing import List, Deque
from queue import Queue

class RecordThread(QtCore.QThread):
    # sig_file_name = pyqtSignal(str)
    RECORD_FPS = 15
    def __init__(self, parent, camera: Camera):
        super().__init__(parent)
        self.__w = 1280
        self.__h = 720
        # self.__imsize_record = (1280, 720)
        # self.__duration = 10  # time before self.duration
        self.__list_event = Queue()  # detection and regconition result
        self.__buffer:deque[Image] = None  # image class buffer
        self.__camera = camera
        self.__logger = get_logger(os.path.join(ROOT, "log", f"{camera.id}_record.log"), mode = "w", name_logger=f"{camera.id}_logger")

    @property
    def buffer(self):
        return self.__buffer

    @buffer.setter
    def buffer(self, buffer: deque):
        self.__buffer = buffer

    @property
    def list_event(self):
        return self.__list_event

    @property
    def camera(self):
        return self.__camera

    @camera.setter
    def camera(self, camera: Camera):
        self.__camera = camera

    @property
    def width(self):
        return self.__w

    @width.setter
    def width(self, w):
        self.__w = w

    @property
    def height(self):
        return self.__h

    @height.setter
    def height(self, h):
        self.__h = h

    @property
    def is_active(self):
        return self.__thread_active

    def _init_arg(self, video_file):
        self._process = (
            ffmpeg
            .input('pipe:0', framerate='{}'.format(self.__camera.record_fps), format='rawvideo', pix_fmt='bgr24',
                   s='{}x{}'.format(int(self.__w), int(self.__h)))
            .output(f'{video_file}', vcodec='h264', pix_fmt='nv21')
            .overwrite_output()
            .run_async(pipe_stdin=True)
            )

    # def setup(self, name_video, id_device, file_path):
    #     self._name_video = name_video
    #     self._id_device = id_device
    #     self._file_path = file_path

    # def __remove_over_duration_element(self, event: BaseResult):
    #     before_duration = event.current_dt.addSecs(-self.__duration)
    #     for e in list(self.__buffer):
    #         if e.time_stamp > before_duration:
    #             break
    #         self.__buffer.remove(e)

    def __get_frame_in_duration(self, event: BaseResult):
        current_time = event.current_dt
        after_duration = current_time.addSecs(1)
        if isinstance(event, PersonResult):
            before_time = current_time.addSecs(-20)
        else:
            before_time = current_time.addSecs(-15)
        arr:List["Image"] = list(filter(lambda x:  (before_time < x.time_stamp) and (x.time_stamp < after_duration), self.__buffer))
        return list(map(lambda x: x.value, arr))

    def __record(self, array_frame, event:BaseResult):
        video_path = event.video_path
        sub_folder = video_path.split("/")[2:]
        sub_folder = "/".join(sub_folder)
        if isinstance(event, PersonResult):
            fullpath = os.path.join(ROOT_SAVE_FILE_PERSON, sub_folder)
            self._init_arg(fullpath)
        else:
            fullpath = os.path.join(ROOT_SAVE_FILE_VEHICLE, sub_folder)
            self._init_arg(fullpath)
        try:
            for frame in array_frame:
                frame = cv2.resize(frame, (self.__w, self.__h))
                self._process.stdin.write(frame.astype(numpy.uint8).tobytes())
            self._process.communicate(str.encode("q"))
            
        except Exception as e:
            print(e)
            if DEBUG_MODE:
                if isinstance(event, PersonResult):
                    self.__logger.error("cannot record for event " + PersonResult.serialize_for_debug(event))
                if isinstance(event,VehicleResult):
                    self.__logger.error("cannot record for event " + VehicleResult.serialize())

    def append_to_event_list(self, event: BaseResult):
        if self.__list_event.qsize() < 100:
            self.__list_event.put(event)

    def run(self):
        self.__thread_active = True
        print('Starting Write Video Thread...')
        while self.__thread_active:
            if self.__list_event.qsize() == 0:  # continue if list event is empty
                time.sleep(0.01)
                continue
            print("Recording")
            event = self.__list_event.get()
            # self.__remove_over_duration_element(event)
            list_frame = self.__get_frame_in_duration(event)
            self.__record(list_frame, event)
            time.sleep(0.001)

    def stop(self):
        self.__thread_active = False
