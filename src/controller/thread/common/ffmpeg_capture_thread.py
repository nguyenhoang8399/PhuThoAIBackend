import cv2
from PyQt5.QtCore import QThread
from queue import Queue
import time
from collections import deque
import ffmpeg
from PyQt5.QtCore import QObject
import numpy as np
from ....model import Camera, Image
from PyQt5.QtCore import QDateTime


class FFMPEGCaptureThread(QThread):
    def __init__(self, parent, camera:Camera):
        super().__init__(parent)
        self.__thread_active = False
        self.__camera = camera
        
        # for record
        self.__max_len_record_buffer = 200 
        self.__out_buffer_record = deque(maxlen=self.__max_len_record_buffer)
        self._previous_link = self.__camera.link
        self._capture_queue = Queue()
        self.__max_out_buffer_size = 100
        
        

    @property
    def out_buffer(self):
        return self._capture_queue

    @property
    def out_buffer_record(self):
        return self.__out_buffer_record

    def setup_cap(self):
        self.__ffmpeg_process = (
            ffmpeg
            .input(self.__camera.link)
            .output('pipe:', format='rawvideo', pix_fmt='rgb24')
            .run_async(pipe_stdout=True)
        )

    def __reconnect(self):
        self.setup_cap()

    def run(self):
        print("Thread Capture start")
        self.__thread_active = True
        self.setup_cap()
        
       
        probe = ffmpeg.probe(self.__camera.link)
        cap_info = next(x for x in probe['streams'] if x['codec_type'] == 'video')
        width = cap_info['width']          
        height = cap_info['height']       
        
        while self.__thread_active:
            in_bytes = self.__ffmpeg_process.stdout.read(width * height * 3)

            if not in_bytes:
                self.__ffmpeg_process.kill()
                time.sleep(1)
                self.__reconnect()
                continue
            
            if self._previous_link != self.__camera.link:
                self.__ffmpeg_process.kill()
                time.sleep(1)
                self.setup_cap()
                print("previous link: ", self._previous_link)
                print("new camera link: ", self.__camera.link)
                
                continue
            
            frame = np.frombuffer(in_bytes, np.uint8).reshape([height, width, 3])
            frame = cv2.cvtColor(frame, cv2.COLOR_RGB2BGR)
            if self._capture_queue.qsize() < self.__max_out_buffer_size:
                self._capture_queue.put(frame)
            

            self.__out_buffer_record.append(Image(frame, QDateTime.currentDateTime()))

            # time.sleep(1.0/self.__camera.capture_fps)
            time.sleep(1.0/self.__camera.capture_fps if "mp4" in self.__camera.link else 0.001)

    def stop(self):
        print('Stoping Capture Thread')
        self.__thread_active = False
        self.__ffmpeg_process.kill()
        if self._capture_queue.qsize != 0:
            self._capture_queue.queue.clear()
        if len(self.__out_buffer_record):
            self.__out_buffer_record.clear()