import cv2
from PyQt5.QtCore import QThread, pyqtSignal
import numpy as np
from queue import Queue
import time
from ....model import Camera, Image
from collections import deque
from PyQt5.QtCore import QDateTime


class CaptureThread(QThread):
    changed_state = pyqtSignal(bool)
    OUTPUT_SIZE_FOR_PROCESS_THREADS = (1920, 1080)
    
    def __init__(self, parent=None, camera: Camera = ...):
        super().__init__(parent)
        self.__thread_active = False
        self.__camera = camera
        
        # for record
        self.__max_len_record_buffer = 200  # for n frames
        self.__out_buffer_record = deque(maxlen=self.__max_len_record_buffer)
        self._previous_link = self.__camera.link
        self._capture_queue = Queue()
        self.__max_out_buffer_size = 30
        
        self.__previous_state = False
        self.__current_state = False

    @property
    def out_buffer(self):
        return self._capture_queue

    @property
    def out_buffer_record(self):
        return self.__out_buffer_record

    def setup_cap(self):
        self._cap = cv2.VideoCapture(self.__camera.link)
        # self._cap.set(cv2.CAP_PROP_BUFFERSIZE, 10)
        # self._cap.set(cv2.CAP_PROP_XI_TIMEOUT, 100)
        self._previous_link = self.__camera.link

    def __reconnect(self):
        self.setup_cap()
        

    def run(self):
        print("Thread Capture start")
        self.__thread_active = True
        self.setup_cap()

        while self.__thread_active:
            self.__previous_state = self.__current_state
            ret, frame = self._cap.read()
            self.__current_state = ret
            
            if not ret:
                self.changed_state.emit(False)
                self._cap.release()
                self.__reconnect()
                time.sleep(2)
                continue
            else:
                if not self.__previous_state:
                    self.changed_state.emit(True)

            if self._previous_link != self.__camera.link:
                self.setup_cap()
                print("previous link: ", self._previous_link)
                print("new camera link: ", self.__camera.link)
                continue
            
            H, W = frame.shape[:2]
            if H != self.OUTPUT_SIZE_FOR_PROCESS_THREADS[1] or W != self.OUTPUT_SIZE_FOR_PROCESS_THREADS[0]:
                frame = cv2.resize(frame, self.OUTPUT_SIZE_FOR_PROCESS_THREADS)
            # frame = cv2.resize(frame, self.OUTPUT_SIZE_FOR_PROCESS_THREADS)
            if self._capture_queue.qsize() < self.__max_out_buffer_size:
                self._capture_queue.put(frame)

            # self.__out_buffer_record.append(Image(frame, QDateTime.currentDateTime()))

            time.sleep(1.0/self.__camera.capture_fps if ".mp4" in self.__camera.link else 0.001)

    def stop(self):
        print('Stoping Capture Thread')
        self.__thread_active = False
        if self._capture_queue.qsize() != 0:
            self._capture_queue.queue.clear()
        # if len(self.__out_buffer_record):
        #     self.__out_buffer_record.clear()