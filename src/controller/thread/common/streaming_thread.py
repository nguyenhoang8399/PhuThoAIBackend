from PyQt5 import *
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
import subprocess
from queue import Queue
import time
import cv2
from ....config import CONST_MEDIA_SERVER_HOST, CONST_STREAMING_SIZE
from ....model import Camera, Polygon

class StreamingThread(QThread):
    def __init__(self, parent, camera: Camera, in_buffer: Queue):
        super().__init__(parent)
        self.__camera = camera
        self.__in_buffer = in_buffer
        self.__thread_active = False
        self.__ffmpeg_process: subprocess.Popen
        self.__previous_rtmp = ""

    @property
    def is_active(self):
        return self.__thread_active

    def __set_args(self, rtmp_link):
        w, h = CONST_STREAMING_SIZE[0], CONST_STREAMING_SIZE[1]
        return (
            "ffmpeg -r 9 -re -stream_loop -1 -f rawvideo -vcodec rawvideo -pix_fmt "
            f"rgb24 -s {w}x{h} -i pipe:0 -pix_fmt yuv420p -c:v libx264 -preset veryfast -tune zerolatency -b:v 2048k "
            f"-f {rtmp_link} -loglevel quiet "
        ).split()

    def __create_process(self, rtmp):
        args = self.__set_args(rtmp)
        return subprocess.Popen(args,shell=True, stdin=subprocess.PIPE)

    def __create_uri_encode(self):
        return f"SVS_{self.__camera.id_company}/{self.__camera.id}{self.__camera.code}"

    def __create_rtmp_link(self):
        uri_encode = self.__create_uri_encode()
        return f"{CONST_MEDIA_SERVER_HOST}/{uri_encode}"

    def __config_frame(self, frame, id_dict):
        frame = frame.copy()
        H, W = frame.shape[:2]
        list_points_format = Polygon.convert_to_point_format(self.__camera.polygon.area_active, W, H)
        cv2.polylines(frame, [list_points_format], True, (0, 255, 0), 2)
        if id_dict.items():
            for k, v in id_dict.items():
                x1, y1, x2, y2, cls = v
                # handle copy frame
                cv2.rectangle(frame, (x1, y1), (x2, y2), (0, 0, 255), 1)
                # cv2.putText(frame, str(k), (x1, y1), cv2.FONT_HERSHEY_SIMPLEX, 4, (0, 0, 255), 2)
        frame = cv2.resize(frame, (CONST_STREAMING_SIZE[0], CONST_STREAMING_SIZE[1]))
        frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        return frame

    def run(self):
        self.__thread_active = True
        rtmp_link = self.__create_rtmp_link()
        print("Starting rtmp stream with rtmp: ", rtmp_link)
        self.__ffmpeg_process = self.__create_process(rtmp_link)
        self.__previous_rtmp = rtmp_link
        while self.__thread_active:
            if self.__in_buffer.qsize() != 0:
                frame, id_dict = self.__in_buffer.get()
                if frame is None:
                    continue
                try:
                    frame = self.__config_frame(frame, id_dict)
                    new_rtmp = self.__create_rtmp_link()
                    if new_rtmp != self.__previous_rtmp:
                        self.__ffmpeg_process.stdin.close()
                        self.__ffmpeg_process.wait()
                        self.__ffmpeg_process = self.__create_process(new_rtmp)
                        self.__previous_rtmp = new_rtmp
                        continue
                    self.__ffmpeg_process.stdin.write(frame.tobytes())
                except Exception as e:
                    print(e)
                    self.__ffmpeg_process.stdin.close()
                    self.__ffmpeg_process.wait()
                    self.__ffmpeg_process = self.__create_process(rtmp_link)
            time.sleep(0.001)
        self.__ffmpeg_process.stdin.close()
        self.__ffmpeg_process.wait()

    def stop(self):
        self.__thread_active = False
