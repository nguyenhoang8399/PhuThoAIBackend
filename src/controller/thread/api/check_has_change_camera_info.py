from PyQt5.QtCore import QThread, pyqtSignal
from ....service import CameraApi, PolygonApi
from ....model import Camera, Polygon
from typing import List
import os
from datetime import datetime
from ....config import ROOT
from ....util import get_logger
from time import sleep
checking_changing_logger = get_logger(os.path.join(ROOT, "log/camera_data_controller.log"))


class CheckHasChange(QThread):
    signal_has_update_camera = pyqtSignal(Camera)

    def __init__(self,parent, source_list):
        super().__init__(parent)
        self.__thread_active = False
        self.__interval = 6
        self.__source_list: List[Camera] = source_list
        self.__incomming_data: List[Camera] = []
        # APIs
        self.__camera_api = CameraApi()
        self.__polygon_api = PolygonApi()


    def check_has_change(self):
        self.__get_camera_data()
        if self.__source_list and self.__incomming_data:
            for e in self.__source_list:
                new_data = self.find_camera_by_id(e.id, self.__incomming_data) 
                if new_data is not None:
                    is_changed = e.is_different_from(new_data)
                    e.merge_data(new_data) #update info
                    if is_changed: # check if any data of camera has change => update info 
                        checking_changing_logger.info("============================")
                        checking_changing_logger.info(f"The camera with id {e.id} has change")
                        checking_changing_logger.info("+++ Old data: ")
                        checking_changing_logger.info(e.toString())
                        self.signal_has_update_camera.emit(e) 
                        checking_changing_logger.info("The camera with id has update")
                        checking_changing_logger.info("+++ New data: ")
                        checking_changing_logger.info(e.toString())
                        checking_changing_logger.info("============================")
                    else:
                        pass
                else:
                    print(checking_changing_logger.info("the data is not in incomming data"))
                    self.__source_list.remove(e)
                    self.signal_has_update_camera.emit(e)

            for ie in self.__incomming_data:
                data_in_source_list = self.find_camera_by_id(ie.id, self.__source_list)
                if data_in_source_list is None:
                    self.__source_list.append(ie)
                    self.signal_has_update_camera.emit(ie)

    def __get_camera_data(self):
        try:
            self.__camera_api.get_access_token()
            data = self.__camera_api.find_all_camera()
            data = data['data']
            self.__incomming_data.clear()
            for e in data:
                camera = Camera.deserialize(e)
                data_polygon = self.__polygon_api.find_polygon_by_id_camera(
                    camera.id)
                
                if len(data_polygon["data"]):
                    polygon = Polygon.deserialize(data_polygon["data"])
                    camera.polygon = polygon
                else:
                    polygon = Polygon.get_default_polygon(camera.id)
                    camera.polygon = polygon
                self.__incomming_data.append(camera)
        except:
            checking_changing_logger.error("Error when get camera data")
            checking_changing_logger.error(datetime.now())        

    @staticmethod
    def find_camera_by_id(id_camera, list_camera: List[Camera]) -> Camera:
        if list_camera:
            for e in list_camera:
                if e.id == id_camera:
                    return e
        return None

    
    def run(self):
        self.__thread_active = True
        while self.__thread_active:
            self.check_has_change()
            sleep(self.__interval)
            
    def stop(self):
        self.__thread_active = False