from PyQt5.QtCore import *
import requests
from ....config import CONST_URL_FACE_ENGINE_UPDATE 
from time import sleep

class UpdateFaceEngineThread(QThread):
    def __init__(self):
        super().__init__()
        self.__interval = 30 # seconds
    
    def __update_face_engine(self):
        try:
            resp = requests.get(CONST_URL_FACE_ENGINE_UPDATE, timeout=2)
            return resp.json()["status"] == 200
        except Exception as er:
            print(er)
            return False

    def run(self):
        self.__thread_active = True
        while self.__thread_active:
            self.__update_face_engine()
            sleep(self.__interval)
        
    def stop(self):
        self.__thread_active = False
    
    