from PyQt5.QtCore import QThread, pyqtSignal
import os 
from queue import Queue
import time
from ....util import get_logger
from ....config import ROOT, DEBUG_MODE
from ....model import VehicleResult
from ....service import VehicleApi
from threading import Thread
from requests.exceptions import ConnectTimeout
from typing import List

class VehicleApiThread(QThread):
    signal_black_list = pyqtSignal(VehicleResult)
    signal_is_posted_to_web_server = pyqtSignal(int)
    
    def __init__(self, parent):
        super().__init__(parent)
        self.__thread_active = False
        self.__veh_api = VehicleApi()
        self.__list_vehicle_rs = Queue()
        self.__max_vehicle_rs = 200
        self.__count_get_access_token = 0
        self.__list_thread:List[Thread] = []
        self.__logger = get_logger(os.path.join(ROOT, "log", "vehicle_api_thread.log"), "vehicle_api_thread")
        
    def append_to_list_vehicle_result(self, rs:VehicleResult):
        # print(rs.digit)
        if self.__list_vehicle_rs.qsize() < self.__max_vehicle_rs:
            self.__list_vehicle_rs.put(rs)
    
    def post_event(self, rs:VehicleResult):
        rs_serialize = VehicleResult.serialize(rs)
        try:
            print("post event: ", rs_serialize)
            resp = self.__veh_api.post_veh_event(rs_serialize)
        except ConnectTimeout as er:
            print(er)
            self.append_to_list_vehicle_result(rs)

        except Exception as er:
            print(er)
            print("Error when post vehicle event: {}".format(resp))
            
    def run(self):
        self.__thread_active = True
        while self.__thread_active:
            if self.__list_vehicle_rs.qsize() > 0:
                rs = self.__list_vehicle_rs.get()
                
                if self.__count_get_access_token % 30 == 0:
                    self.__veh_api.get_access_token()
                
                new_thread = Thread(target=self.post_event, args=(rs,))
                new_thread.start()
                self.__list_thread.append(new_thread)
                self.__count_get_access_token += 1
                
                for e in self.__list_thread:
                    e.join()
                    if not e.is_alive():
                        self.__list_thread.remove(e)
            time.sleep(0.01)
   
    def stop(self):
        self.__thread_active = False
        
       