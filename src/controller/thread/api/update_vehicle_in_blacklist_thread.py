from PyQt5.QtCore import *
from ....service import VehicleApi
from time import sleep

class UpdateVehicleInBlacklistThread(QThread):
    signal_has_update = pyqtSignal()
    def __init__(self):
        super().__init__()
        self.__interval = 5 # seconds
        self.__vehicle_api = VehicleApi()
        self.__vehicle_in_blacklist = []
    
    @property
    def vehicle_in_blacklist(self):
        return self.__vehicle_in_blacklist
    
    def __update_vehicle_in_blacklist(self):
        try:
            data = self.__vehicle_api.find_all_vehicle_in_black_list()
            if data:
                self.__vehicle_in_blacklist[:] = data
        except Exception as er:
            print(er)

    def run(self):
        self.__thread_active = True
        while self.__thread_active:
            # self.__vehicle_api.get_access_token()
            # self.__update_vehicle_in_blacklist()
            self.signal_has_update.emit()
            sleep(self.__interval)
        
    def stop(self):
        self.__thread_active = False
    
    