from PyQt5.QtCore import QThread, pyqtSignal
import os 
from queue import Queue
import time
from ....util import get_logger
from ....config import ROOT, DEBUG_MODE
from ....model import PersonResult
from ....service import PersonEventApi
from threading import Thread
from requests.exceptions import ConnectTimeout
from typing import List


class PersonApiThread(QThread):
    signal_black_list = pyqtSignal(PersonResult)
    signal_is_posted_to_web_server = pyqtSignal(int)
    
    def __init__(self, parent):
        super().__init__(parent)
        self.__thread_active = False
        self.__person_api = PersonEventApi()
        self.__list_person_rs = Queue()
        self.__max_person_rs = 200
        self.__count_get_access_token = 0
        self.__list_thread:List[Thread] = []
        self.__logger = get_logger(os.path.join(ROOT, "log", "vehicle_api_thread.log"), "vehicle_api_thread")
        
        
    def append_to_list_person_result(self, rs:PersonResult):
        if self.__list_person_rs.qsize() < self.__max_person_rs:
            self.__list_person_rs.put(rs)
    
    def post_event(self, rs:PersonResult):
        rs_serialize = PersonResult.serialize(rs)
        try:
            resp = self.__person_api.post_person_event(rs_serialize)
            if resp:
                self.signal_is_posted_to_web_server.emit(rs.tracked_id)
              
        except ConnectTimeout as er:
            self.append_to_list_person_result(rs)

        except Exception as er:
            print(er)
            print("Error when post vehicle event: {}".format(resp))
            
    def run(self):
        self.__thread_active = True
        while self.__thread_active:
            if self.__list_person_rs.qsize() > 0:
                rs = self.__list_person_rs.get()
                
                if self.__count_get_access_token % 30 == 0:
                    self.__person_api.get_access_token()
                
                new_thread = Thread(target=self.post_event, args=(rs,), daemon=True)
                new_thread.start()
                self.__list_thread.append(new_thread)
                
                for e in self.__list_thread:
                    if not e.is_alive():
                        self.__list_thread.remove(e)
                self.__count_get_access_token += 1
            time.sleep(0.01)
   
    def stop(self):
        self.__thread_active = False
        
       