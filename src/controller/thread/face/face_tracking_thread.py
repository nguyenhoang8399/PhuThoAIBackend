from PyQt5.QtCore import QThread, pyqtSignal, pyqtSlot, QDateTime
import cv2
from queue import Queue
import os
import numpy as np
from ....config import ROOT, DEBUG_MODE
from ....model import Camera, Polygon, Image
from ....util import get_logger, Tracking, concate_image
from ....yolov5 import FaceClassify
import imutils
from .person_object_manager import PersonObjectManager
from .person_object import PersonObject, CropedFaceFrame
import time
from collections import deque


class FaceTrackingThread(QThread):
    signal_person_object = pyqtSignal(PersonObject)

    LIMIT_FACE_RATIO = (0.03, 0.03)
    MAX_SIZE_PAINT_EVENT_BUFFER = 3
    MAX_SIZE_STREAMING_BUFFER = 3
    MAX_SIZE_OUT_BUFFER = 300
    MAX_SIZE_RECORD_BUFFER = 200
    
    def __init__(self, parent, camera, in_buffer):
        super().__init__(parent)
        self.__thread_active = False
        self.__in_buffer: Queue = in_buffer
        self.__camera = camera

        # === CONFIG OUT BUFFER ===
        self.__paint_event_buffer = Queue()
        self.__stream_buffer = Queue()
        self.__tracker = Tracking()
        self.__set_ids_is_posted_to_web_server = set()
        self.__set_ids_is_posted_to_frs = set()
        self.__out_buffer = Queue()
        self.__out_buffer_record = deque(maxlen=self.MAX_SIZE_RECORD_BUFFER)        

        self.__classifier = FaceClassify()
        self.__tracked_person_manager = PersonObjectManager()
        self.__tracked_person_manager.sig_person_need_to_handle.connect(self.append_to_out_buffer)
        self._logger = get_logger(os.path.join(
            ROOT, "log/tracking_thread.log"), "w", "face_tracking_thread")

    @property
    def out_buffer(self):
        return self.__out_buffer
    
    @property
    def paint_event_buffer(self):
        return self.__paint_event_buffer

    @property
    def stream_buffer(self):
        return self.__stream_buffer

    @property
    def is_active(self):
        return self.__thread_active

    @property
    def camera(self):
        return self.__camera

    @camera.setter
    def camera(self, camera: Camera):
        self.__camera = camera

    @property
    def person_object_manager(self):
        return self.__tracked_person_manager

    @property
    def out_buffer_record(self):
        return self.__out_buffer_record

    @staticmethod
    def append_to_set_tracked_id(set_ids_in_frame: set, id_in_frame):
        if len(set_ids_in_frame) > 3000:
            for i in range(0, 500):
                set_ids_in_frame.pop()
        set_ids_in_frame.add(id_in_frame)



    @pyqtSlot(int)
    def append_tracked_id_posted_to_web_server(self, tracked_id):
        """ add to set of ids that were posted to web server
        """
        self.append_to_set_tracked_id(
            self.__set_ids_is_posted_to_web_server, tracked_id)

    @pyqtSlot(int)
    def append_tracked_id_posted_to_frs(self, tracked_id: int):
        """ add to set of ids that were posted to face recognition server
        """
        self.append_to_set_tracked_id(
            self.__set_ids_is_posted_to_frs, tracked_id)


    def append_to_out_buffer(self, tracked_person: PersonObject):
        if self.__out_buffer.qsize() < self.MAX_SIZE_OUT_BUFFER:
            if isinstance(tracked_person, PersonObject):
                self.__out_buffer.put(tracked_person)

    def __is_posted_to_web_server(self, tracked_id):
        return tracked_id in self.__set_ids_is_posted_to_web_server

    def __is_posted_to_frs(self, tracked_id):
        return tracked_id in self.__set_ids_is_posted_to_frs

    def setup_tracker_model(self):
        self.__tracker.weights = os.path.join(ROOT, "resources/Weight/face_v3.engine")
        self.__tracker.imgsz = 320
        self.__tracker.device = "0"
        self.__tracker.conf_thres = 0.4
        self.__tracker.classes = [1]
        self.__tracker.agnostic_nms = True
        self.__tracker.half = True
        self.__tracker._load_model()

    def setup_classifier_model(self):
        self.__classifier.weights = os.path.join(
            ROOT, "resources/Weight/good_face_classify.pt")
        self.__classifier.imgsz = [224, 224]
        self.__classifier.device = "0"
        self.__classifier.half = False
        self.__classifier.load_model()

    def __check_quality_ratio_of_face(self, bbox, frame):
        x1, y1, x2, y2 = bbox
        w, h = x2 - x1, y2 - y1
        W, H = frame.shape[1], frame.shape[0]
        return w / W > self.LIMIT_FACE_RATIO[0] and h / H > self.LIMIT_FACE_RATIO[1]

    def __expand_bbox(self, frame: np.ndarray, bbox: tuple):
        x1, y1, x2, y2 = bbox
        w, h = x2 - x1, y2 - y1
        H, W, _ = frame.shape
        x1 = max(0, x1 - w // 10)
        y1 = max(0, y1 - h // 10)
        x2 = min(W, x2 + w // 10)
        y2 = min(H, y2 + h // 10)
        return x1, y1, x2, y2

    def run(self):
        print(f"Start Tracking Thread in camera {self.__camera.id}")
        self.setup_tracker_model()
        self.setup_classifier_model()

        self.__thread_active = True
        count = 0
        fps = 0
        old_time = time.time()
        while self.__thread_active:
            if time.time() - old_time > 1:
                fps = count
                count = 0
                old_time = time.time()
                
            if self.__in_buffer.qsize() < 1:
                time.sleep(0.001)
                continue
            start_time = time.time()
            frame = self.__in_buffer.get()
            # frame = cv2.resize(frame, (1920, 1080))
            frame_copy = frame.copy()
            H, W = frame.shape[:2]
            list_points_format = Polygon.convert_to_point_format(
                self.__camera.polygon.area_active, W, H)
            

            track_dict = self.__tracker.track(frame)
            for tracked_id, bbox in track_dict.items():
                x1, y1, x2, y2 = bbox[:4]
                center_x, center_y = (x1 + x2) // 2, (y1 + y2) // 2
                center = (center_x, center_y)  # center of bounding box
                is_in_polygon = cv2.pointPolygonTest(list_points_format, center, False)
                tracked_person = self.__tracked_person_manager.find_person_with_tracked_id(tracked_id)
                
                if is_in_polygon > 0:
                    x1, y1, x2, y2 = self.__expand_bbox(frame_copy, bbox[:4])
                    # ------- DRAW RECTANGLE AND TEXT ON FRAME -------
                    if self.__is_posted_to_frs(tracked_id):
                        if self.__is_posted_to_web_server(tracked_id):
                            frame_copy = cv2.rectangle(
                                frame_copy, (x1, y1), (x2, y2), (0, 255, 0), 4)
                            cv2.putText(
                                frame_copy, f"{tracked_id} RECOGNITING", (x1, y1), cv2.FONT_HERSHEY_SIMPLEX, 1, ((0, 255, 0)))
                        else:
                            frame_copy = cv2.rectangle(
                                frame_copy, (x1, y1), (x2, y2), (255, 0, 0), 4)
                            cv2.putText(
                                frame_copy, f"{tracked_id} POSTED TO WEB", (x1, y1), cv2.FONT_HERSHEY_SIMPLEX, 1, ((0, 255, 0)))
                    else:
                        frame_copy = cv2.rectangle(
                            frame_copy, (x1, y1), (x2, y2), (0, 0, 255), 4)
                        cv2.putText(
                            frame_copy, f"{tracked_id} CHECKING", (x1, y1), cv2.FONT_HERSHEY_SIMPLEX, 1, ((0, 255, 0)))

                    # ------- END DRAW RECTANGLE AND TEXT ON FRAME -------
                    
                    if tracked_person is None:
                        tracked_person = PersonObject(tracked_id)
                        self.__tracked_person_manager.append_person(tracked_person)
                        
                    tracked_person.last_time_in_plate_zone = QDateTime.currentSecsSinceEpoch()

                    # ------- CHECK QUALITY OF FACE -------
                    if tracked_id in self.__set_ids_is_posted_to_web_server:
                        continue

                    if not self.__check_quality_ratio_of_face(bbox[:4], frame_copy):
                        continue

                    if (y2 - y1) / (x2 - x1) > 2:  # in case person is misundertood as face
                        continue

                    # apply classify for detect face
                    face_crop = frame[y1:y2, x1:x2]
                    # face_crop_copy = face_crop.copy()
                    cls, accuracy = self.__classifier.predict(face_crop)
                    
                    if cls != 0:
                        continue

                    # ------- END CHECK QUALITY OF FACE -------

                    face_crop = imutils.resize(face_crop, width=128)
                    # face_dict[tracked_id] = (face_crop, accuracy)

                    if not tracked_person.is_posted_to_web_server:
                        tracked_person.add_face_crop(face_crop, accuracy)
                    
                else:
                    if tracked_person is not None:
                        tracked_person.is_in_polygon = False
                
                if tracked_person is not None:    
                    if (tracked_person.len_face_crop() > 20) or (not tracked_person.is_in_polygon):    
                        # self.signal_person_object.emit(tracked_person)
                        self.__out_buffer.put(tracked_person)

                

            #paint event
            # cv2.putText(frame_copy, f"FPS: {fps}", (100, 100), cv2.FONT_HERSHEY_SIMPLEX, 2, (0, 255, 0), 2)
            count += 1
            cv2.polylines(frame_copy, [list_points_format], True, (0, 0, 255), 2)

            # if self.__paint_event_buffer.qsize() < self.MAX_SIZE_PAINT_EVENT_BUFFER:
            #     self.__paint_event_buffer.put(frame_copy)
            
            self.__out_buffer_record.append(Image(frame, QDateTime.currentDateTime()))

            if self.__stream_buffer.qsize() < self.MAX_SIZE_STREAMING_BUFFER:
                self.__stream_buffer.put((frame_copy, {}))
                
            time.sleep(0.001)

    def stop(self):
        self.__thread_active = False
        if len(self.__out_buffer_record):
            self.__out_buffer_record.clear()
        print("Stop Tracking Thread")
