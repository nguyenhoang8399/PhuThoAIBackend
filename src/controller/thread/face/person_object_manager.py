from typing import List
from PyQt5.QtCore import QDateTime, QObject, QTimer, pyqtSignal
from ....util import get_logger
from ....config import ROOT, DEBUG_MODE
import os
from .person_object import PersonObject, PersonInBlackList
from collections import deque

class PersonObjectManager(QObject):
    sig_person_need_to_handle = pyqtSignal(PersonObject)
    
    MAX_ALLOWED_MISSING_TIME = 1 # seconds
    
    def __init__(self):
        super().__init__()
        self._list_person_cache:List[PersonObject] = []
        
        
        self.check_missing_person_cache = QTimer()
        self.check_missing_person_cache.setInterval(100)
        self.check_missing_person_cache.timeout.connect(self.check_missing_person_and_remove_posted_to_web_person)
        self.check_missing_person_cache.start()
        
        self.check_person_is_satified_for_handle = QTimer()
        self.check_person_is_satified_for_handle.setInterval(1000)
        self.check_person_is_satified_for_handle.timeout.connect(self.emit_person_need_to_handle)
        self.check_person_is_satified_for_handle.start()

    def check_missing_person_and_remove_posted_to_web_person(self):
        for person in self._list_person_cache:
            duration = QDateTime.currentSecsSinceEpoch() - person.last_time_in_plate_zone
            person.is_missing = duration > self.MAX_ALLOWED_MISSING_TIME # consider missing if not tracked for MAX_ALLOWED_MISSING_TIME seconds in polygon
        
        for person in self._list_person_cache:
            if person.is_posted_to_web_server:
                self._list_person_cache.remove(person)
            
        self.__force_del_tracked_object()

    def __force_del_tracked_object(self):
        for k in self._list_person_cache:
            last_time_in_plate_zone = k.last_time_in_plate_zone
            max_allowed_time_in_cache = 5
            if QDateTime.currentSecsSinceEpoch() - last_time_in_plate_zone > max_allowed_time_in_cache:
                self._list_person_cache.remove(k)
            
    def emit_person_need_to_handle(self):
        for person in self._list_person_cache:
            condition = person.is_missing and (not person.is_posted_to_web_server) and (not person.is_recognized)
            if condition:
                self.sig_person_need_to_handle.emit(person)
                
    def find_person_with_tracked_id(self, id_in_frame) -> PersonObject:
        if self._list_person_cache:
            for e in self._list_person_cache:
                if e.tracked_id == id_in_frame:
                    return e
        return None
    
    def append_person(self, person:PersonObject):
        self._list_person_cache.append(person)
        
    def remove_person(self, person:PersonObject):
        self._list_person_cache.remove(person)
    

class PersonInBlacklistManager:
    MAX_ALLOWED_DURATION_IN_BLACKLIST = 1 # seconds
    
    def __init__(self):
        self.list_person_in_blacklist = deque(maxlen=1000)
        self._max_allowed_duration_in_blacklist = 2*60 # seconds
        
    def add_person_in_blacklist(self, person_in_blacklist:PersonInBlackList):
        self.list_person_in_blacklist.append(person_in_blacklist)
        
    def find_person_in_blacklist(self, person_id) -> PersonInBlackList:
        if self.list_person_in_blacklist:
            for e in self.list_person_in_blacklist:
                if e.person_id == person_id:
                    return e
        return None
    
    def is_in_allowed_time(self, person_id):
        person = self.find_person_in_blacklist(person_id)
        if person is not None:
            duration = QDateTime.currentSecsSinceEpoch() - person.time_stamp
            return duration < self.MAX_ALLOWED_DURATION_IN_BLACKLIST
        return False