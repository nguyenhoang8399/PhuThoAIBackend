from typing import List
from PyQt5.QtCore import QDateTime, QObject
import numpy as np
from ....util import concate_image, recommend_row_col

class CropedFaceFrame:
    def __init__(self, frame, accuracy):
        self.frame = frame
        self.accuracy = accuracy

class FrameRecognition:
    def __init__(self, frame, response):
        self.frame = frame
        self.response = response
        self.quality = response["data"]["biometric"]["quality"]

class PersonObject(QObject):
    # CONSTANTS
    NUMBER_CROPED_FACE_TO_HANDLE = 9
    MAX_NUMBER_CROPED_FACE = 20
    
    def __init__(self, tracked_id):
        super().__init__()
        self.tracked_id = tracked_id
        self.put_to_face_queue_time_stamp = QDateTime.currentSecsSinceEpoch()
        self.is_blacklist = False
        self.person_id = None
        
        self.is_in_polygon = True
        self.list_croped_face:List[CropedFaceFrame] = []
        self.list_recognized_face:List[FrameRecognition] = []
        
        self.is_posted_to_web_server = False
        self.is_missing = False 
        self.is_recognized = False
        self.last_time_in_plate_zone = QDateTime.currentSecsSinceEpoch()
        

    def find_frame_with_max_quality(self):
        list_quality = [e.accuracy for e in self.list_croped_face]
        max_quality = max(list_quality)
        
        for frame in self.list_croped_face:
            if frame.accuracy == max_quality:
                return frame
        
    def add_face_crop(self, frame, accuracy):
        self.list_croped_face.append(CropedFaceFrame(frame, accuracy))
        # self.list_croped_face = sorted(self.list_croped_face, key=lambda x: x.accuracy, reverse=True)
    
    
    def add_face_recognition(self, frame, resp):
        self.list_recognized_face.append(FrameRecognition(frame, resp))
        self.list_recognized_face = sorted(self.list_recognized_face, key=lambda x: x.quality, reverse=True)
        
    def len_face_crop(self):
        return len(self.list_croped_face)

    def len_face_recognition(self):
        return len(self.list_recognized_face)
    
    @staticmethod
    def __find_best_accuracy_in_croped_face(list_croped_face:List[CropedFaceFrame], n_face_per_image) -> List[np.ndarray]:
        # len_faces = len(list_croped_face)
        if len(list_croped_face) < n_face_per_image:
            return [croped_face.frame for croped_face in list_croped_face]
        else:
            new_faces = []
            len_faces = len(list_croped_face)
            for i in range(n_face_per_image):
                face = list_croped_face[int(i * len_faces // n_face_per_image): int((i + 1) * len_faces // n_face_per_image)]
                new_faces.append(face[0].frame)
            return new_faces
    
    @staticmethod
    def concate_image(images):
        bbox_locate_list = []
        # bbox_dict = {"x": "", "y": "", "width": "", "height": ""}
        max_width = sorted(images, key=lambda x: x.shape[1], reverse=True)[0].shape[1]
        max_height = sorted(images, key=lambda x: x.shape[0], reverse=True)[0].shape[0]
        row, col = recommend_row_col(len(images))
        black_image = np.zeros((max_height * row, max_width * col, 3), np.uint8)
        if row == 1 and col == 1:
            return (images[0], [{"x": 0, "y": 0, "width": images[0].shape[1], "height": images[0].shape[0]}])
        count = 0
        for i in range(row):
            for j in range(col):
                if count < len(images):
                    image = images[count]
                    black_image[i * max_height:i * max_height + image.shape[0],
                                j * max_width:j * max_width + image.shape[1]] = image
                    bbox_dict = {}
                    bbox_dict["x"] = j * max_width
                    bbox_dict["y"] = i * max_height
                    bbox_dict["width"] = image.shape[1]
                    bbox_dict["height"] = image.shape[0]
                    bbox_locate_list.append(bbox_dict.copy())
                    count += 1
        return (black_image, bbox_locate_list)
    
    def get_concated_image_from_face_frames(self):
        faces = self.__find_best_accuracy_in_croped_face(self.list_croped_face, self.NUMBER_CROPED_FACE_TO_HANDLE)
        result = concate_image(faces)
        if len(result) == 2:
            image, bbox_locate_list = result
        else:
            image = result
            bbox_locate_list = [] # in the case the only one face in the concated image
        return (image, bbox_locate_list)
    
    def check_condition_for_face_recognition(self):
        condition = (self.len_face_crop() > 20) or (not self.is_in_polygon) or self.is_missing or (not self.is_posted_to_web_server)
       
        return condition
        
class PersonInBlackList:
    def __init__(self, person_id, time_stamp):
        self.person_id = person_id
        self.time_stamp = time_stamp
        
        