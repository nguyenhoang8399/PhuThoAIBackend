from PyQt5.QtCore import QThread, QDateTime
from PyQt5.QtCore import pyqtSignal, QUuid
from threading import Thread
from queue import Queue
from time import time
import time
import os
import cv2
from typing import List
from datetime import datetime
from .person_object_manager import  PersonObjectManager, PersonInBlacklistManager
from .person_object import PersonObject, PersonInBlackList

from ....service import FaceRecogApi, PersonEventApi
from ....util import img_2_base64, create_folder_on_another_computer, get_logger, base64_2_img
from ....model import PersonResult, Camera
from ....config import ROOT_SAVE_FILE_PERSON, ROOT, DEBUG_MODE
from collections import deque
import numpy as np
from datetime import datetime
import platform


class FaceRecognitionThread(QThread):
    signal_black_list = pyqtSignal(PersonResult)
    signal_save_image = pyqtSignal(tuple)
    signal_tracked_id_posted_to_web_server = pyqtSignal(int)
    signal_tracked_id_posted_to_frs = pyqtSignal(int)
    signal_person_info = pyqtSignal(PersonResult)
    MIN_QUALITY = 0.2   
    
    def __init__(self, parent, camera: Camera, in_buffer: Queue):
        super().__init__(parent)
        self.__thread_active = False
        self.__camera = camera

        # config face queue for process
        self.__tracked_person_buffer = in_buffer # save face queue for process
        self.__max_size_queue = 300
        self.__person_object_manager:PersonObjectManager
        self.__person_in_black_list_manager = PersonInBlacklistManager()
        self.__person_api = PersonEventApi()
        self.__list_thread:List[Thread] = []
        self.__list_tracked_id_posted_to_web = deque(maxlen=200)
        self.__server_name = platform.node()
        
        self.__logger = get_logger(os.path.join(ROOT, "log/connect_to_frs_thread.log"), "w", "connect_to_frs_thread")
   
    @property
    def is_active(self):
        return self.__thread_active

    @property
    def person_object_manager(self):
        return self.__person_object_manager
    
    @person_object_manager.setter
    def person_object_manager(self, person_object_manager: PersonObjectManager):
        self.__person_object_manager = person_object_manager
        self.__person_object_manager.sig_person_need_to_handle.connect(self.append_to_tracked_person_buffer)


    def append_to_tracked_person_buffer(self, tracked_person: PersonObject):
        if self.__tracked_person_buffer.qsize() < self.__max_size_queue:
            # print("append_to_tracked_person_buffer", tracked_person.tracked_id, tracked_person.is_missing)
            # print("size of tracked person buffer: ", self.__tracked_person_buffer.qsize())
            if isinstance(tracked_person, PersonObject):
                self.__tracked_person_buffer.put(tracked_person)
        

    @staticmethod
    def __create_payload_for_face_recognize(base64, bbox_locate_list):
        payload = {
            "base64": base64,
            "compIds": [1],
            "threshold": 0.75,
            "img_info": bbox_locate_list
        }
        return payload

    @staticmethod
    def get_letter_path(prefix):
        return os.path.abspath(prefix)[0]

    def __create_result_from_face_recog_server(self, face_crop):
        rs = PersonResult()
        rs.camera_id = self.__camera.id
        rs.event_id = QUuid.createUuid().toString(QUuid.StringFormat.WithoutBraces)
        date_folder = datetime.now().strftime('%d_%m_%Y')
        img_folder = os.path.join(ROOT_SAVE_FILE_PERSON, "images/" + date_folder)
        video_folder = os.path.join(ROOT_SAVE_FILE_PERSON, "videos/" + date_folder)

        # Create folder by date if not exist
        create_folder_on_another_computer(img_folder)
        create_folder_on_another_computer(video_folder)

        # save image
        prefix_path = self.get_letter_path(ROOT_SAVE_FILE_PERSON) + "_{}".format(self.__server_name)

        img_file = os.path.join(ROOT_SAVE_FILE_PERSON,
                                f"images/{date_folder}/" + rs.event_id + ".jpg")
        # cv2.imwrite(img_file, face_crop)
        self.signal_save_image.emit((img_file, face_crop))
        rs.img_path = f"{prefix_path}/person/images/{date_folder}/{rs.event_id}.jpg"
        rs.video_path = f"{prefix_path}/person/videos/{date_folder}/{rs.event_id}.mp4"
        return rs


    def do_recognize(self, tracked_person: PersonObject):
        tracked_person.is_recognized = True
        tracked_id = tracked_person.tracked_id
        concated_image, bbox_locate_list = tracked_person.get_concated_image_from_face_frames()
        concated_image_base64 = img_2_base64(concated_image)
        
        payload = self.__create_payload_for_face_recognize(concated_image_base64, bbox_locate_list)
        resp = FaceRecogApi.recognize_face(payload)
        
        if resp["status"] != 200:
            print(resp)
            return
        
        self.signal_tracked_id_posted_to_frs.emit(tracked_id)
        biometric = resp["data"]["biometric"] # to get feature of face
        
        # Draw the face in face_crop
        face_crop = base64_2_img(biometric["imageBase64"])
        
        # create result to post to web
        rs = self.__create_result_from_face_recog_server(face_crop)
        rs.tracked_id = tracked_id
        rs.features, rs.gender, rs.age = biometric["feature"], biometric["gender"], biometric["age"]

        if resp["data"]["result"]: # if result contain person in blacklist
            list_matched_person = sorted(resp["data"]["result"], key=lambda x: x["scoreMatching"], reverse=True)
            data = list_matched_person[0] # get the best scoreMatching
            rs.person_id = data["personId"]
            rs.current_dt = QDateTime.currentDateTime()
            self.signal_black_list.emit(rs)
            
            old_person_in_blacklist = self.__person_in_black_list_manager.find_person_in_blacklist(rs.person_id)
            if old_person_in_blacklist is not None:
                if not self.__person_in_black_list_manager.is_in_allowed_time(rs.person_id):
                    self.signal_person_info.emit(rs)
                    tracked_person.is_posted_to_web_server = True
                    old_person_in_blacklist.time_stamp = QDateTime.currentSecsSinceEpoch() # update time stamp if posted to web server
            else: # new personid in cache
                self.__person_in_black_list_manager.add_person_in_blacklist(PersonInBlackList(rs.person_id, QDateTime.currentSecsSinceEpoch()))
                self.signal_person_info.emit(rs)
                tracked_person.is_posted_to_web_server = True
                
        else:
            rs.current_dt = QDateTime.currentDateTime()
            # new_thread = Thread(target=self.post_event, args=(rs, tracked_id,), daemon=True)
            # new_thread.start()
            self.signal_person_info.emit(rs)
            tracked_person.is_posted_to_web_server = True
        self.__list_tracked_id_posted_to_web.append(tracked_id)
    

    def run(self):
        self.__thread_active = True
        print("starting conntect to frs thread")
        while self.__thread_active:
            # Continue if face queue is empty
            time.sleep(0.001)
            if self.__tracked_person_buffer.qsize() == 0:
                self.msleep(20)
                continue

            tracked_person:PersonObject = self.__tracked_person_buffer.get()
            
            if tracked_person is None:
                continue
            
            if not isinstance(tracked_person, PersonObject):
                continue
            
            tracked_id = tracked_person.tracked_id
            
            if tracked_id in self.__list_tracked_id_posted_to_web:
                continue
           
            if not tracked_person.len_face_crop():
                continue
            
            if tracked_person.is_recognized:
                continue
            
            print("Recognizing person: {}".format(tracked_id))
            t = Thread(target=self.do_recognize, args=(tracked_person,))
            t.start()
            for e in self.__list_thread:
                e.join()
                if not e.is_alive():
                    self.__list_thread.remove(e)
            # ================ END HANDLE RESPONSE ================
            
    def stop(self):
        print("Stop Api Thread")
        self.__thread_active = False

    # def post_event(self, result:PersonResult, tracked_id):
    #     rs_serialize = PersonResult.serialize(result)
    #     self.__person_api.get_access_token()
    #     resp = self.__person_api.post_person_event(rs_serialize)
    #     self.signal_tracked_id_posted_to_web_server.emit(tracked_id)
    #     if DEBUG_MODE:
    #         self.__logger.info("**************************")
    #         self.__logger.info("PAYLOAD: {}".format(PersonResult.serialize_for_debug(result)))
    #         self.__logger.info(f"POST EVENT: {resp}")
    #         self.__logger.info("**************************")
