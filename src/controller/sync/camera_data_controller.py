from PyQt5.QtCore import QObject, pyqtSignal
from ...config import ROOT
from ...model import Camera
from typing import List
import os
from ..thread import CheckHasChange


class CameraDataController(QObject):
    signal_has_update_camera = pyqtSignal(Camera)
    
    def __init__(self, source_list: List[Camera]):
        super().__init__()
        # Lists to be compared
        self.__check_has_change_thread = CheckHasChange(self, source_list)
        
        self.__check_has_change_thread.signal_has_update_camera.connect(
            self.has_update)
        
        self.destroyed.connect(self.__check_has_change_thread.terminate)

    def has_update(self, camera):
        self.signal_has_update_camera.emit(camera)

    def start_thread(self):
        self.__check_has_change_thread.start()
