from PyQt5.QtCore import QObject, pyqtSignal, pyqtSlot, QTimer
from PyQt5.QtWidgets import QFileDialog, QMessageBox, QWidget, QShortcut
from PyQt5.QtGui import QKeySequence
import os
import shutil
from ...config import (ROOT_SAVE_FILE_PERSON, 
                       ROOT_SAVE_FILE_VEHICLE, 
                       ROOT_SAVE_FILE, 
                       PERCENTAGE_TO_WARNING,
                       PERCENTAGE_TO_DELETE_FOLDER,
                       HOUR_TO_RUN_CHECKING,
                       MINUTE_TO_RUN_CHECKING,
                       NUMBER_OF_DAYS
                       )

from ...template import Ui_WgDataManager
from .recalculate_thread import ReCalculateThread
from .table_data_manager import TableDataManagerController
from multiprocessing import Process
from typing import List
from datetime import datetime, timedelta
from PyQt5.QtGui import QIcon
from PyQt5 import QtGui
from threading import Thread

class WgDataManagerController(QWidget):
    def __init__(self):
        super().__init__()
        self.__ui = Ui_WgDataManager()
        self.__ui.setupUi(self)
        self.setWindowTitle("Quản lý dữ liệu")
        icon = QIcon()
        icon.addPixmap(QtGui.QPixmap(":/icons/resources/icons/update.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.setWindowIcon(icon)
        self.table_controller = TableDataManagerController(self.__ui.table_folder)
        
        self.__list_process_detele:List[Process] = []
        self.__connect_signals()
        self.__create_shortcut()
        
   
        time_to_start = self.__calculate_time_to_start(HOUR_TO_RUN_CHECKING, MINUTE_TO_RUN_CHECKING)
        print("time to start: ", time_to_start)
       
            
    def __connect_signals(self):
        
        # self.__calculate_capacity_thread.signal_size_folder.connect(self.__handle_size_of_folder)
        pass
    
        
    def __create_shortcut(self):
        self._esc_shortcut = QShortcut(QKeySequence("Escape"), self)
        self._esc_shortcut.activated.connect(self.close)            
 
    @staticmethod
    def list_all_folder_data(prefix):
        return os.listdir(prefix)

    @staticmethod
    def remove_folder_data(prefix):
        if os.path.isdir(prefix):
            shutil.rmtree(prefix)
       
      
    @staticmethod
    def __calculate_time_to_start(hour:int, minute:int):
        x = datetime.today()
        time_to_start = datetime(x.year, x.month, x.day, hour, minute)
        print("time_to_start: ", time_to_start)
        print("x: ", x)
        
        if time_to_start < x:
            y = x.replace(day=x.day, hour=hour, minute=minute, second=0, microsecond=0) + timedelta(days=1)
        else:
            y = x.replace(day=x.day, hour=hour, minute=minute, second=0, microsecond=0)  
        delta_t=y-x
        secs=delta_t.total_seconds()
        
        return secs
        