from datetime import datetime
import os
import shutil


class FolderObj:
    def __init__(self, name, prefix):
        """
        Args:
            name (str): name of folder
            prefix (str): point to parent of parent of the folder. 
                            eg. ../storage_yb/vehicle/images/2020_01_01 => prefix = ../storage_yb/vehicle
        """
    
        self.name = name
        self.prefix = prefix
        self.video_path = os.path.join(prefix, "videos", name)
        self.image_path = os.path.join(prefix, "images", name)
        self.date = self.convert_to_date_folder_name(name)
        self.is_delete = False
        
    
    @staticmethod
    def convert_to_date_folder_name(folder_name):
        try:
            return datetime.strptime(folder_name, '%d_%m_%Y').date()
        except ValueError:
            return None
  
    def delete_folder(self):
        print("deleting video folder: ", self.video_path)
        print("deleting image folder: ", self.image_path)
        self.remove_folder_data(self.video_path)
        self.remove_folder_data(self.image_path)
        print("Done deleting video folder: ", self.video_path)
        print("Done deleting image folder: ", self.image_path)
        self.is_delete = True
    
    @staticmethod
    def remove_folder_data(prefix):
        if os.path.isdir(prefix) and os.path.exists(prefix):
            shutil.rmtree(prefix)
            

    def __repr__(self):
        return f"FolderObj({self.name}, {self.prefix})"
        
    
        
    
    