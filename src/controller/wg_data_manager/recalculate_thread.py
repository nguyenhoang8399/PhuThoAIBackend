from PyQt5.QtCore import QThread, pyqtSignal
import shutil
import os
from .folder_obj import FolderObj
from typing import List
from ...config import PERCENTAGE_TO_DELETE_FOLDER, NUMBER_OF_DAYS
from threading import Thread
import time


class ReCalculateThread(QThread):
    signal_size_folder = pyqtSignal(float, float, float) # size_of_disk, total_size_of_vehicle_folder, total_size_of_person_folder
    is_finished = pyqtSignal(bool)
    
    def __init__(self, parent, prefix):
        super().__init__(parent)
        self.__prefix = prefix
        self.__vechile_prefix = os.path.join(prefix, "vehicle")
        self.__person_prefix = os.path.join(prefix, "person")
        
        self.list_dir_vehicle = []
        self.list_dir_person = []
        
        self.list_vehicle_folder_obj:List[FolderObj] = []
        self.list_person_folder_obj:List[FolderObj] = []    
        
        self.disk_size = 0
        self.vehicle_size = 0
        self.person_size = 0
        
        self.total_size_of_disk = 0
        self.free_size_of_disk = 0
        self.interval_time = 10*60 # 10 minutes
        
        self.thread_active = False
    
    def convert_list_folder_to_folder_obj(self, list_folder, prefix):
        value = []
        for e in list_folder:
            new_folder_obj = FolderObj(e, prefix)
            value.append(new_folder_obj)
        return value
                
    def run(self):
        self.thread_active = True
        
        while self.thread_active:
            self.is_finished.emit(False)
            print("starting check capacity thread")
            self.total_size_of_disk = shutil.disk_usage(self.__prefix).total
            self.free_size_of_disk = shutil.disk_usage(self.__prefix).free
            
            usage_percent = round((self.total_size_of_disk - self.free_size_of_disk) / self.total_size_of_disk * 100, 2)
            print("usage_percent: ", usage_percent)
            
            
            vehicle_images_prefix = os.path.join(self.__vechile_prefix, "images")
            vehicle_videos_prefix = os.path.join(self.__vechile_prefix, "videos")
            person_images_prefix = os.path.join(self.__person_prefix, "images")
            person_videos_prefix = os.path.join(self.__person_prefix, "videos")
            
            list_date_image_folder_vehicle = os.listdir(vehicle_images_prefix)
            list_date_image_folder_person = os.listdir(person_images_prefix)
            
            self.list_dir_vehicle[:] = list_date_image_folder_vehicle
            self.list_dir_person[:] = list_date_image_folder_person
            
            self.list_vehicle_folder_obj[:] = self.convert_list_folder_to_folder_obj(self.list_dir_vehicle, self.__vechile_prefix)
            self.list_person_folder_obj[:] = self.convert_list_folder_to_folder_obj(self.list_dir_person, self.__person_prefix)
            
            self.list_person_folder_obj.sort(key=lambda x: x.date, reverse=True)
            self.list_vehicle_folder_obj.sort(key=lambda x: x.date, reverse=True)
            
            total_size_of_vehicle_folder = 0
            total_size_of_person_folder = 0
            
            # for vfo in self.list_vehicle_folder_obj:
            #     video_path = vfo.video_path
            #     image_path = vfo.image_path
            #     total_size_of_vehicle_folder += self.find_size_of_folder(video_path) + self.find_size_of_folder(image_path)
                
                
            # for plf in self.list_person_folder_obj:
            #     video_path = plf.video_path
            #     image_path = plf.image_path
            #     total_size_of_person_folder += self.find_size_of_folder(video_path) + self.find_size_of_folder(image_path)
                
            # Delete folder if usage percent > 90%
            
            
            size_of_disk = self.find_size_of_disk(self.__prefix)
            self.signal_size_folder.emit(size_of_disk, total_size_of_vehicle_folder, total_size_of_person_folder)
            self.disk_size = size_of_disk
            self.vehicle_size = total_size_of_vehicle_folder
            self.person_size = total_size_of_person_folder
            self.is_finished.emit(True)
            if usage_percent > PERCENTAGE_TO_DELETE_FOLDER:
                self.__detele_oldest_folder()
            time.sleep(self.interval_time)

    def __detele_oldest_folder(self):
        number_of_folder_to_delete = NUMBER_OF_DAYS
        if len(self.list_vehicle_folder_obj) < number_of_folder_to_delete or \
            len(self.list_person_folder_obj) < number_of_folder_to_delete:
            number_of_folder_to_delete = 1
        
        list_deleted_vehicle_folder = self.list_vehicle_folder_obj[-NUMBER_OF_DAYS:]
        list_deleted_person_folder = self.list_person_folder_obj[-NUMBER_OF_DAYS:]
        print("list_deleted_vehicle_folder: ", list_deleted_vehicle_folder)
        print("list_deleted_person_folder: ", list_deleted_person_folder)
        for e in list_deleted_vehicle_folder:
            delete_vehicle_folder_thread = Thread(target=e.delete_folder)
            delete_vehicle_folder_thread.start()
            delete_vehicle_folder_thread.join()
            
        for e in list_deleted_person_folder:
            delete_person_folder_thread = Thread(target=e.delete_folder)
            delete_person_folder_thread.start()  
            delete_vehicle_folder_thread.join()
    
    @staticmethod
    def find_size_of_folder(prefix):
        if os.path.isdir(prefix):
            size = 0
            for ele in os.scandir(prefix):
                size += os.path.getsize(ele)
            return round(size/1000/1000/1000, 4)
        else:
            return 0
        
    @staticmethod
    def find_size_of_disk(prefix):
        size = shutil.disk_usage(prefix).total
        size_in_GB = round(size/1000/1000/1000, 4)
        # return "{} GB".format(round(size_in_GB, 2)) if size_in_GB > 1 else "{} MB".format(size_in_GB * 1000)
        return size_in_GB
    
    def stop(self):
        self.thread_active = False
