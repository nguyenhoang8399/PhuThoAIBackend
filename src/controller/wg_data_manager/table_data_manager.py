from PyQt5.QtCore import QObject
from PyQt5.QtWidgets import QCheckBox, QTableWidgetItem, QTableWidget
from typing import List
from .folder_obj import FolderObj


class TableDataManagerController(QObject):
    def __init__(self, table):
        super().__init__()
        self.__table:QTableWidget = table
        self.__list_folder_obj:List[FolderObj] = []
        self.__list_row_items:List[FolderRowItem] = []
    
    @property
    def list_folder_obj(self):
        return self.__list_folder_obj
    
    @list_folder_obj.setter
    def list_folder_obj(self, value):
        self.__list_folder_obj[:] = value
    
    def reupdate_table(self):
        self.clear_table()
        self.load_table()
        
    def clear_table(self):
        self.__table.setRowCount(0)
        self.__list_row_items.clear()

    def load_table(self):
        self.convert_list_camera_to_row_item()
        self.update_table()  
    
    def convert_list_camera_to_row_item(self):
        for e in self.__list_folder_obj:
            row_item = FolderRowItem(e)
            self.__list_row_items.append(row_item)
        
    def insert_row_to_position(self, position:int, row_item:"FolderRowItem"):
        self.__table.insertRow(position)
        self.__table.setCellWidget(position, 0, row_item.checkbox)
        self.__table.setItem(position, 1, row_item.folder_name)

    def update_table(self):
        if self.__list_row_items:
            for i, v in enumerate(self.__list_row_items):
                self.insert_row_to_position(i, v)
        
    def find_all_chosen_folder(self):
        rs = []
        if self.__list_row_items:
            for e in self.__list_row_items:
               if e.is_chosen: rs.append(e.folder_name_str)
        return rs
    
    
class FolderRowItem(QObject):
    def __init__(self, folder_model:FolderObj):
        super().__init__()
        self.checkbox = QCheckBox()
        self.checkbox.toggled.connect(self.on_checked)
        self.is_chosen = self.checkbox.isChecked()
        self.folder_name_str = folder_model.name
        self.folder_name = QTableWidgetItem(self.folder_name_str)
        
    def on_checked(self, state):
        self.is_chosen = state
        