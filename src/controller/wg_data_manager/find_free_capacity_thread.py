

class FindFreeSpaceThread(QThread):
    def __init__(self, parent=None):
        super(FindFreeSpaceThread, self).__init__(parent)
        self.parent = parent

    def run(self):
        self.parent.find_free_space()