from threading import Thread
from PyQt5.QtGui import QKeySequence, QCloseEvent, QIcon
from PyQt5.QtWidgets import QGridLayout, QMainWindow, QShortcut, QFrame, QPushButton
from PyQt5 import QtCore
from ..wg_camera_manager import WgCameraManagerController
from ..wg_data_manager import WgDataManagerController
from ..wg_camera import PersonWgCamera, VehicleWgCamera, BaseWgCamera
from ..config_interface import MainBtnConfig
from ..thread import UpdateFaceEngineThread, UpdateVehicleInBlacklistThread
from ..wg_camera import ListWgsController
from ...model import Camera
from ...template import Ui_MainWindow
from ...util import recommend_row_col
from ...service import CameraApi
from ...util import get_logger
from ...config import ROOT
import os
import time
import src.resources
from typing import List
import sys
import asyncio
from ...util import read_json_data

main_logger = get_logger(os.path.join(ROOT, "log/main_app.log"))


class MainWindowController(QMainWindow):
    def __init__(self):
        super().__init__()
        self.__ui = Ui_MainWindow()
        self.__ui.setupUi(self)
        self.setWindowTitle("GIÁM SÁT AN NINH THÀNH PHỐ YÊN BÁI")
        # self.setWindowIcon(QIcon(":/icons/resources/icons/ATIN_logo-01.png"))
        self.setup_layout_camera()
        
        # wg camera manager
        self.__wg_camera_manager = WgCameraManagerController()
        self.__list_cameras = self.__wg_camera_manager.list_cameras
        self.__list_wgs:List[BaseWgCamera] = []
        self.__list_wg_controller = ListWgsController(self.__list_wgs)
        self.__list_face_cameras = self.__wg_camera_manager.list_face_cameras
        self.__list_vehicle_cameras:List[Camera] = self.__wg_camera_manager.list_vehicle_cameras
        
        # Update face engine thread and person in blacklist
        self.__update_face_engine_thread = UpdateFaceEngineThread()
        # self.__update_face_engine_thread.start()
        
        # Update vehicle in blacklist
        self.__update_vehicle_in_blacklist_thread = UpdateVehicleInBlacklistThread()
        # self.__update_vehicle_in_blacklist_thread.start()
        self.__update_vehicle_in_blacklist_thread.signal_has_update.connect(self.reload_vehicle_in_blacklist)
        
        # Wg Data Manager
        self.__wg_data_manager = WgDataManagerController()
        
        # setup interface and connect signals
        self.create_shortcuts()
        self.connect_wg_camera_controller_signals()
        self.connect_btn_and_action_signals()
        
        self._camera_api = CameraApi()
        self.__wg_camera_manager.update_camera_info_from_db()
        self.__showing_vehicle_monitor()
        self.__update_wg_camera()
        self.__read_state_camera_cache()
        
    
    def create_shortcuts(self):
        # Close shortcut
        self._close_sc = QShortcut(QKeySequence("Ctrl+C"), self)
        self._close_sc.activated.connect(self.close)
        
        # Open camera manager shortcut
        self._open_camera_manager_sc = QShortcut(QKeySequence("Ctrl+Q"), self)
        self._open_camera_manager_sc.activated.connect(self.reopen_camera_manager)
        
        # Open camera manager shortcut
        self._open_data_manager_sc = QShortcut(QKeySequence("Ctrl+D"), self)
        self._open_data_manager_sc.activated.connect(self.reopen_data_manager)
    
    def connect_wg_camera_controller_signals(self):
        self.__wg_camera_manager.signal_start_all_camera.connect(self.start_all_camera_wg)
        self.__wg_camera_manager.signal_start_camera.connect(self.start_camera)
        self.__wg_camera_manager.signal_stop_camera.connect(self.stop_camera)
        # self.__wg_camera_manager.signal_update_db.connect(self.__update_wg_camera)
        self.__wg_camera_manager.signal_has_update_camera.connect(self.update_info_camera)
        
    def reopen_camera_manager(self):
        self.__wg_camera_manager.show()
        self.__wg_camera_manager.raise_()
        
    def reopen_data_manager(self):
        self.__wg_data_manager.show()
        self.__wg_data_manager.raise_()
        
    def connect_btn_and_action_signals(self):
        self.__ui.actionDanh_s_ch_thi_t_b.triggered.connect(self.reopen_camera_manager)
        self.__ui.btn_camera_manager.clicked.connect(self.reopen_camera_manager)
        self.__ui.btn_veh_monitor.clicked.connect(self._show_layout_veh_mornitor)
        self.__ui.btn_face_monitor.clicked.connect(self._show_layout_face_mornitor)
        self.__ui.btn_restart_app.clicked.connect(self.restart)
        self.__ui.btn_data_manager.clicked.connect(self.reopen_data_manager)

    # setup layout
    def setup_layout_camera(self):
        self._grid_layout_camera_frame = QGridLayout()
        self.__ui.qframe_camera.setLayout(self._grid_layout_camera_frame)
        
        # face mornitor qframe
        self.__frame_face_monitor = QFrame()
        self.__grid_layout_person_cameras = QGridLayout()
        self.__grid_layout_person_cameras.setContentsMargins(10, 10, 10, 10)
        self.__grid_layout_person_cameras.setVerticalSpacing(10)
        self.__grid_layout_person_cameras.setHorizontalSpacing(10)
        self.__frame_face_monitor.setLayout(self.__grid_layout_person_cameras)
    
        # vehicle mornitor qframe
        self.__frame_vehicle_mornitor = QFrame()
        self.__grid_layout_vehicle_cameras = QGridLayout()
        self.__grid_layout_vehicle_cameras.setContentsMargins(10, 10, 10, 10)
        self.__grid_layout_vehicle_cameras.setVerticalSpacing(10)
        self.__grid_layout_vehicle_cameras.setHorizontalSpacing(10)
        self.__frame_vehicle_mornitor.setLayout(self.__grid_layout_vehicle_cameras)
   
    def __update_wg_camera(self):
        self.__init_face_camera()
        self.__init_vehicle_camera()
   
    def __init_face_camera(self):
        if self.__list_face_cameras:
            cols_face, rows_face = recommend_row_col(len(self.__list_face_cameras))
            count_face = 0
            for ri in range(0, rows_face):
                for ci in range(0, cols_face):
                    if count_face >= len(self.__list_face_cameras):
                        break
                    new_wg = PersonWgCamera(self.__list_face_cameras[count_face])
                    self.__grid_layout_person_cameras.addWidget(new_wg, ri, ci)
                    self.__list_wgs.append(new_wg)
                    count_face +=1
   
    def __init_vehicle_camera(self):
        if self.__list_vehicle_cameras:
            cols_face, rows_face = recommend_row_col(len(self.__list_vehicle_cameras))
            count_vechile = 0
            for ri in range(0, rows_face):
                for ci in range(0, cols_face):
                    if count_vechile >= len(self.__list_vehicle_cameras):
                        break
                    new_wg = VehicleWgCamera(self.__list_vehicle_cameras[count_vechile])
                    new_wg.vehicle_in_blacklist = self.__update_vehicle_in_blacklist_thread.vehicle_in_blacklist
                    self.__grid_layout_vehicle_cameras.addWidget(new_wg, ri, ci)
                    self.__list_wgs.append(new_wg)
                    count_vechile += 1

    def __read_state_camera_cache(self):
        data = read_json_data(WgCameraManagerController.OLD_STATE_CACHE_FILE)
        for e in self.__list_cameras:
            if str(e.id) in data:
                if data[str(e.id)]:
                    self.start_camera(e)
                    e.old_state = True
                    self.__wg_camera_manager.set_state_link(e, "ON")
                    self.__wg_camera_manager.disable_start_button_in_row_table_by_camera(e)
                
    def start_all_camera_wg(self): 
        self.__list_wg_controller.start_all_wgs()

    def __showing_vehicle_monitor(self):
        self.__ui.btn_veh_monitor.setStyleSheet(MainBtnConfig.CLICKED)
        # show veh qframe monitor
        self.__frame_vehicle_mornitor.setHidden(False)
        self._grid_layout_camera_frame.addWidget(self.__frame_vehicle_mornitor, 0, 0)

    def _show_layout_veh_mornitor(self):
        #hidden current frame
        self._grid_layout_camera_frame.removeWidget(self.__frame_face_monitor)
        self.__frame_face_monitor.setHidden(True)
        
        # handle chosen frame button
        button:QPushButton = self.sender()
        self.__ui.btn_face_monitor.setStyleSheet(MainBtnConfig.NORMAL)
        button.setStyleSheet(MainBtnConfig.CLICKED)
        
        # show veh qframe monitor
        self.__frame_vehicle_mornitor.setHidden(False)
        self._grid_layout_camera_frame.addWidget(self.__frame_vehicle_mornitor, 0, 0)
    
    def _show_layout_face_mornitor(self):
        #hidden current frame
        self._grid_layout_camera_frame.removeWidget(self.__frame_vehicle_mornitor)
        self.__frame_vehicle_mornitor.setHidden(True)
        self.__ui.btn_veh_monitor.setStyleSheet(MainBtnConfig.NORMAL)
        
        # handle chosen frame button
        button:QPushButton = self.sender()
        button.setStyleSheet(MainBtnConfig.CLICKED)
        
        # show face qframe moninor
        self.__frame_face_monitor.setHidden(False)
        self._grid_layout_camera_frame.addWidget(self.__frame_face_monitor, 0, 0)
    
    def start_camera(self, camera:Camera):
        self.__list_wg_controller.start_wg(camera)
        self.update_status_camera_to_web_server(camera, 1)
        
    def stop_camera(self, camera:Camera):
        self.__list_wg_controller.stop_wg(camera)
        self.update_status_camera_to_web_server(camera, 2)
    
    def put_status_camera(self, camera_id, status):
        self._camera_api.get_access_token()
        self._camera_api.put_status_camera(camera_id, status)
    
    def update_status_camera_to_web_server(self, camera:Camera, status):
        new_thread = Thread(target=self.put_status_camera, args=(camera.id, status))
        new_thread.start()
    
    @staticmethod
    def remove_all_wg(layout:QGridLayout):
        for i in reversed(range(layout.count())): 
            layout.itemAt(i).widget().setParent(None)
    
    def reupdate_grid_layout(self):
        list_indexs_face_wgs = self.__list_wg_controller.find_indexs_of_wg_by_function(camera_function=1)
        list_indexs_vehicle_wgs = self.__list_wg_controller.find_indexs_of_wg_by_function(camera_function=2)
        n_face_wg = len(list_indexs_face_wgs)
        n_vehicle_wg = len(list_indexs_vehicle_wgs)
        
        self.remove_all_wg(self.__grid_layout_person_cameras)
        self.remove_all_wg(self.__grid_layout_vehicle_cameras)
        
        # === Face wgs handle ===
        if n_face_wg != 0:
            cols_face, rows_face = recommend_row_col(n_face_wg)
            count_face = 0
            for ri in range(0, rows_face):
                for ci in range(0, cols_face):
                    if count_face >= n_face_wg:
                        break
                    self.__grid_layout_person_cameras.addWidget(self.__list_wgs[list_indexs_face_wgs[count_face]], ri, ci)
                    count_face +=1
                
        # === Vehicle wgs handle ===
        if n_vehicle_wg != 0:
            cols_vehicle, rows_vehicle = recommend_row_col(n_vehicle_wg)
            count_vehicle = 0
            for ri in range(0, rows_vehicle):
                for ci in range(0, cols_vehicle):
                    if count_vehicle >= n_vehicle_wg:
                        break
                    self.__grid_layout_vehicle_cameras.addWidget(self.__list_wgs[list_indexs_vehicle_wgs[count_vehicle]], ri, ci)
                    count_vehicle +=1

    def remove_old_wg_camera(self, old_wg:BaseWgCamera, grid_layout:QGridLayout):
        old_wg.stop_thread()
        time.sleep(2)
        grid_layout.removeWidget(old_wg)
        self.__list_wgs.remove(old_wg)
    
    def update_info_camera(self, camera:Camera):
        self.__wg_camera_manager.filter_by_function()
        old_wg = self.__list_wg_controller.find_wgs_by_camera(camera)
        old_camera = self.__wg_camera_manager.find_camera_by_id(camera.id) # check camera is exist in list camera
        # if old_camera is None, it means this camera is deleted on Web
        if old_camera is None:
            wg = self.__list_wg_controller.find_wgs_by_camera(camera)
            if isinstance(wg, PersonWgCamera):
                self.remove_old_wg_camera(wg, self.__grid_layout_person_cameras)
            else:
                self.remove_old_wg_camera(wg, self.__grid_layout_vehicle_cameras)
            self.reupdate_grid_layout()
            return
            
        if old_wg is not None:
            if camera.function == 1 and (not isinstance(old_wg, PersonWgCamera)):
                self.remove_old_wg_camera(
                    old_wg, self.__grid_layout_vehicle_cameras)
                new_wg = PersonWgCamera(camera)
                self.__list_wgs.append(new_wg)
                self.update_status_camera_to_web_server(camera, 2)
                self.__wg_camera_manager.disable_stop_button_in_row_table_by_camera(camera)
                self.reupdate_grid_layout()
                return

            if camera.function == 2 and (not isinstance(old_wg, VehicleWgCamera)):
                self.remove_old_wg_camera(
                    old_wg, self.__grid_layout_person_cameras)
                new_wg = VehicleWgCamera(camera)
                new_wg.vehicle_in_blacklist = self.__update_vehicle_in_blacklist_thread.vehicle_in_blacklist
                self.__list_wgs.append(new_wg)
                self.update_status_camera_to_web_server(camera, 2)
                self.__wg_camera_manager.disable_stop_button_in_row_table_by_camera(camera)
                self.reupdate_grid_layout()
                return
        else:
            main_logger.info("New camera info is fetch")
            main_logger.info("let update")
            new_wg = PersonWgCamera(camera) if camera.function == 1 else VehicleWgCamera(camera)
            self.__list_wgs.append(new_wg)
            self.reupdate_grid_layout()
            return
    
    def reload_vehicle_in_blacklist(self):
        for wg in self.__list_wgs:
            if isinstance(wg, VehicleWgCamera):
                wg.vehicle_in_blacklist = self.__update_vehicle_in_blacklist_thread.vehicle_in_blacklist
  
    
    def terminate_all_running_thread(self):
        self.__list_wg_controller.stop_all_wgs() 
        self.__update_face_engine_thread.terminate()
        for e in self.__list_cameras:
            self.update_status_camera_to_web_server(e, 2) # send close status to web server
       
    def closeEvent(self, a0: QCloseEvent) -> None:
        self.__wg_camera_manager.close()
        self.terminate_all_running_thread()
            
    def restart(self):
        self.close()
        self.__wg_camera_manager.close()
        QtCore.QCoreApplication.quit()
        status = QtCore.QProcess.startDetached(sys.executable, sys.argv)
