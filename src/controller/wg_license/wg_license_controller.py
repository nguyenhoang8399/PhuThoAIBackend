from PyQt5.QtCore import QObject
from PyQt5.QtWidgets import QDialog
from ...template import Ui_WgLicense
import hashlib
from getmac import get_mac_address as gma
import os
from ...config import ROOT
from ...util import write_yaml_file, read_yaml_file

class WgLicenseController(QDialog):
    LICENSE_FILE = os.path.join(ROOT, 'license', 'license.yaml')
    
    def __init__(self):
        super().__init__()
        self._ui = Ui_WgLicense()
        self._ui.setupUi(self)
        self.__is_valid = False
        self._ui.buttonBox.accepted.connect(self.on_accepted)
        self.value = ""

    @property
    def is_valid(self):
        return self.__is_valid
        
    def on_accepted(self):
        self.value = self._ui.license_key_input.text()
        self.__is_valid = self.check_license(self.value)
        self.save_license_value()    
  
    @staticmethod
    def check_license(value):
        secret_key = 'ATIN2023'
        str_to_hash:str = gma() + ":" + secret_key
        hashed_value = hashlib.sha256(str_to_hash.encode("utf-8")).hexdigest()
        hashed_value = hashed_value[:16]
        hashed_value = hashed_value.upper()
        return value == hashed_value
    
    def save_license_value(self):
        dict_data = {
            "key": self.value
        }
        write_yaml_file(self.LICENSE_FILE, dict_data)
    
    def load_license_cache(self):
        dict_data = read_yaml_file(self.LICENSE_FILE)
        self.value = dict_data['key']
    
    def check_license_file(self):
        is_exist_file = self.check_exist_file()
        if is_exist_file:
            self.load_license_cache()
            return self.check_license(self.value)
        else:
            return False
    
    def check_exist_file(self):
        file_path = self.LICENSE_FILE
        return os.path.exists(file_path)
    
