from PyQt5.QtCore import pyqtSignal
from PyQt5.QtWidgets import QWidget, QShortcut, QMessageBox
from PyQt5.QtGui import QKeySequence
from ...model import Camera
from ...template import Ui_CameraManager
from .table_controller import TableController
from ...service import PolygonApi, CameraApi
from ...model import Polygon
from ..sync import CameraDataController
from typing import List

class WgCameraManagerController(QWidget):
    signal_start_all_camera = pyqtSignal()
    signal_config_record = pyqtSignal()
    signal_update_db = pyqtSignal()

    signal_start_camera = pyqtSignal(Camera)
    signal_stop_camera = pyqtSignal(Camera)
    signal_has_update_camera = pyqtSignal(Camera)
    OLD_STATE_CACHE_FILE = TableController.OLD_STATE_CACHE_FILE
    
    def __init__(self):
        super().__init__()
        self._ui = Ui_CameraManager()
        self._ui.setupUi(self)
        self.setWindowTitle("Quản lý Camera")
        self._ui.btn_update_camera.hide()
        self.__list_cameras:List[Camera] = []
        self.__list_vehicle_cameras = []
        self.__list_face_cameras = []
        
        self.__table_controller = TableController(self._ui.tableWidget, self.__list_cameras)
        self.__camera_api = CameraApi()
        self.__polygon_api = PolygonApi()
        self.create_shortcut()

        # Update from db
        self._camera_data_controller = CameraDataController(self.__list_cameras)
        self._camera_data_controller.signal_has_update_camera.connect(self.has_update)
        self.connect_signal()

    @property
    def list_cameras(self):
        return self.__list_cameras

    @property
    def list_face_cameras(self):
        return self.__list_face_cameras

    @property
    def list_vehicle_cameras(self):
        return self.__list_vehicle_cameras

    def has_update(self, camera):
        self.signal_has_update_camera.emit(camera)
        self.__table_controller.re_update(camera)

    def create_shortcut(self):
        self._esc_shortcut = QShortcut(QKeySequence("Escape"), self)
        self._esc_shortcut.activated.connect(self.close)

    def connect_signal(self):
        self._ui.btn_update_camera.clicked.connect(self.update_camera_info_from_db)
        self.__table_controller.signal_start_camera.connect(self.on_start_camera)
        self.__table_controller.signal_stop_camera.connect(self.on_stop_camera)

    def disable_start_button_in_row_table_by_camera(self, camera: Camera):
        self.__table_controller.disable_start_button_in_row(camera)

    def disable_stop_button_in_row_table_by_camera(self, camera: Camera):
        self.__table_controller.disable_stop_button_in_row(camera)
  
    def update_camera_info_from_db(self):
        try: 
            self.fetch_data_camera()
        except:
            print("Error fetch data camera")
            self.show_mess_box("Lỗi", "Không thể kết nối đến server", "error")
            return
        self.__table_controller.load_table()
        # self._camera_data_controller.start_thread()

    def fetch_data_camera(self):
        data = self.__camera_api.find_all_camera()
        print(data)
        data = data['items']
        for e in data:
            camera = Camera.deserialize(e)
            polygon = Polygon.get_default_polygon(camera.id)
            camera.polygon = polygon
            self.__list_cameras.append(camera)
        
        self.filter_by_function()
        
    def filter_by_function(self):
        # self.__list_face_cameras[:] = [e for e in self.__list_cameras if e.function == 1]
        self.__list_vehicle_cameras[:] = [e for e in self.__list_cameras if e.function == 1]
        
    def on_start_camera(self, camera: Camera):
        print(f"{camera.id} is start")
        print(f"Link camera id {camera.link}")
        self.signal_start_camera.emit(camera)

    def on_stop_camera(self, camera):
        print(f"{camera.id} is stop")
        self.signal_stop_camera.emit(camera)
        
    def find_camera_by_id(self, id:int):
        if self.__list_cameras:
            for e in self.__list_cameras:
                if e.id == id:
                    return e
        return None
    
    def show_mess_box(self, title, message, type):
        msgBox = QMessageBox()
        if type == "error":
            msgBox.setIcon(QMessageBox.Information)
        msgBox.setText(message)
        msgBox.setWindowTitle(title)
        msgBox.setStandardButtons(QMessageBox.Ok)
        returnValue = msgBox.exec()
        if returnValue == QMessageBox.Ok:
            msgBox.close()

    def disable_start_camera(self, camera:Camera):
        self.__table_controller.disable_start_button_in_row(camera)
        
    def disable_stop_camera(self, camera):
        self.__table_controller.disable_stop_button_in_row(camera)
        
    def set_state_link(self, camera:Camera, state_str:str):
        self.__table_controller.set_state_link(camera, state_str)