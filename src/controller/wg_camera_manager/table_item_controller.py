from PyQt5.QtCore import QObject, pyqtSignal, Qt, QThread
from PyQt5.QtWidgets import QTableWidgetItem, QPushButton
from PyQt5.QtGui import QCursor, QColor
from ...model import Camera
from ...util import check_status_camera
from time import sleep
from threading import Thread
from ...config import ROOT
import os
import json
from  ...util import read_json_data, write_json_data

class RowItem(QObject):
    OLD_STATE_CACHE_FILE = os.path.join(ROOT, "resources/state_camera", "old_state.json")
    signal_start = pyqtSignal(Camera)
    signal_stop = pyqtSignal(Camera)
    signal_edit = pyqtSignal(Camera)

    def __init__(self, camera: Camera):
        super().__init__()
        self.__camera = camera

        self.status = QTableWidgetItem("OFF")
        self.id_camera = QTableWidgetItem(str(self.__camera.id))
        self.camera_code = QTableWidgetItem(str(self.__camera.code))
        self.name = QTableWidgetItem(str(self.__camera.name))
        self.function = QTableWidgetItem(
            "Giám sát đối tượng" if self.__camera.function == 1 else "Giám sát phương tiện")
        self.link = QTableWidgetItem(self.__camera.link)
        self.state_link = QTableWidgetItem(
            "Đang kiểm tra")  # Hợp lệ & Không hợp lệ

        # init buttons
        self.button_start = Button(self.__camera)
        self.button_start.setText("Start")
        self.button_start.setCursor(QCursor(Qt.PointingHandCursor))
        self.button_start.signal_camera.connect(self.on_clicked_start)
        self.button_start.setEnabled(False)

        self.button_stop = Button(self.__camera)
        self.button_stop.setText("Stop")
        self.button_stop.setCursor(QCursor(Qt.PointingHandCursor))
        self.button_stop.signal_camera.connect(self.on_clicked_stop)
        self.button_stop.setEnabled(False)

        self.button_edit_info = Button(self.__camera)
        self.button_edit_info.setText("Edit")
        self.button_edit_info.setCursor(QCursor(Qt.PointingHandCursor))
        self.button_edit_info.signal_camera.connect(self.on_clicked_edit)

        self._is_running = False
        self.button_start.setEnabled(True)
        
    @property
    def camera(self):
        return self.__camera    
    
    def disable_start_button(self):
        self.button_start.setEnabled(False)
    
    def disable_stop_button(self):
        self.button_stop.setEnabled(False)
        
    def enable_start_button(self):
        self.button_start.setEnabled(True)
    
    def enable_stop_button(self):
        self.button_stop.setEnabled(True)

    def reupdate_new_camera_info(self):
        self.link.setText(self.__camera.link)
        self.camera_code.setText(self.__camera.name)
        self.function.setText("Giám sát đối tượng" if self.__camera.function == 1 else "Giám sát phương tiện")
    
    def on_valid_link(self, is_valid = True):
        try:
            if not self._is_running:
                self.button_start.setEnabled(is_valid)
            if is_valid:
                self.state_link.setText("Hợp lệ")
            else:
                self.state_link.setText("Không hợp lệ")
        except RuntimeError:
            print("The row item is removed")

    def on_clicked_start(self, camera: Camera):
        self.signal_start.emit(camera)
        self.sender().setEnabled(False)
        self.button_stop.setEnabled(True)
        self._is_running = True
        self.__camera.old_state = True
        self.set_state_for_cache(True)
        self.set_state_link("ON")
 
    def on_clicked_stop(self, camera: Camera):
        self.signal_stop.emit(camera)
        self.sender().setEnabled(False)
        self.button_start.setEnabled(True)
        self._is_running = False
        self.__camera.old_state = False
        self.set_state_for_cache(False)
        self.set_state_link("OFF")
        
    def set_state_for_cache(self, state):
        old_data = self.read_state()
        old_data[str(self.__camera.id)] = state
        self.save_state(old_data)
    
    def set_state_link(self, state_str):
        self.state_link.setText(state_str)
        self.state_link.setForeground(QColor("red") if state_str == "OFF" else QColor("green"))
        
    def read_state(self):
        return read_json_data(self.OLD_STATE_CACHE_FILE)
    
    def save_state(self, payload):
        write_json_data(self.OLD_STATE_CACHE_FILE, payload)

    def on_clicked_edit(self, camera: Camera):
        self.signal_edit.emit(camera)


class Button(QPushButton):
    signal_camera = pyqtSignal(Camera)

    def __init__(self, camera: Camera):
        super().__init__()
        self.__camera = camera
        self.clicked.connect(lambda: self.signal_camera.emit(self.__camera))
        self.translate()

    def translate(self):
        self.setFixedWidth(80)
        self.setFixedHeight(30)

class CheckValidLink(QThread):
    signal_state_camera = pyqtSignal(bool)

    def __init__(self,parent:QTableWidgetItem,  camera: Camera):
        super().__init__(parent)
        self.__camera = camera
        self.__interval = 3
        self.__thread_active = False
        
    def run(self):
        
        self.__thread_active = True
        while self.__thread_active:
            status_camera = check_status_camera(self.__camera.link)
            self.signal_state_camera.emit(status_camera)
            sleep(self.__interval)

    def stop(self):
        self.__thread_active = False
