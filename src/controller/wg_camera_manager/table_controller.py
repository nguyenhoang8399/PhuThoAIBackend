from PyQt5.QtCore import QObject, pyqtSignal
from PyQt5.QtWidgets import QTableWidget, QAbstractScrollArea
from ...model import Camera
from typing import List
from .table_item_controller import RowItem
from ...util import get_logger
from ...config import ROOT
import os
table_logger = get_logger(os.path.join(ROOT, "log/table_controller.log"))
from ...util import read_json_data


class TableController(QObject):
    signal_start_camera = pyqtSignal(Camera)
    signal_stop_camera = pyqtSignal(Camera)
    signal_edit_camera = pyqtSignal(Camera)
    OLD_STATE_CACHE_FILE = RowItem.OLD_STATE_CACHE_FILE
    
    def __init__(self, table:QTableWidget, list_camera:List[Camera]):
        super().__init__()
        self.__table = table
        self.__table.resizeColumnsToContents()
        self.__table.verticalHeader().setVisible(False)
        self.__table.setSizeAdjustPolicy(QAbstractScrollArea.AdjustToContents)
        
        self.__list_camera = list_camera
        self.__list_row_items:List[RowItem] = []
    
    def load_table(self):
        self.convert_list_camera_to_row_item()
        self.update_table()    
        # self.start_all_thread_in_row_items()
    
    def disable_start_button_in_row(self, camera:Camera):
        row_item = self.find_row_by_camera(camera)
        if row_item is not None:
            row_item.disable_start_button()
            row_item.enable_stop_button()
            row_item.set_state_for_cache(True)
            
    def disable_stop_button_in_row(self, camera:Camera):
        row_item = self.find_row_by_camera(camera)
        if row_item is not None:
            row_item.disable_stop_button()
            row_item.enable_start_button()
            row_item.set_state_for_cache(False)
    
    def re_update(self, camera):
        table_logger.info(" ======== ")
        table_logger.info(f"Camera with id {camera.id} ")
        row_item = self.find_row_by_camera(camera)
        
        if camera not in self.__list_camera: # camera is remove in self.__list_camera
            if row_item is not None: # if row item with the camera still exist -> remove in self.__list_row_item
                # Handle camera is delete event
                row_index = self.__list_row_items.index(row_item)
                table_logger.info(f"camera with id {camera.id} is remove")
                self.__table.removeRow(row_index)
                self.__list_row_items.remove(row_item)
        else:
            # Handle camera is update info event
            if row_item is not None:
                table_logger.info("--------")
                table_logger.info(f"update camera info with id {camera.id}")
                self.handle_update_new_camera_info()
                return
            else:
                # Handle new camera event
                table_logger.info("=========")
                table_logger.info(f"camera with id {camera.id} is added")
                new_row_item = RowItem(camera)
                table_logger.info("current len of list_row_item is {}".format(len(self.__list_row_items)))
                self.__list_row_items.append(new_row_item)  
                self.insert_row_to_position(0, new_row_item)
                # new_row_item.start_check_state_link()

    def insert_row_to_position(self, position:int, row_item:RowItem):
        self.__table.insertRow(position)
        self.__table.setItem(position, 0, row_item.id_camera)
        self.__table.setItem(position, 1, row_item.camera_code)
        self.__table.setItem(position, 2, row_item.name)
        self.__table.setItem(position, 3, row_item.function)
        self.__table.setItem(position, 4, row_item.link)
        self.__table.setItem(position, 5, row_item.state_link)
        self.__table.setCellWidget(position, 6, row_item.button_start)
        row_item.signal_start.connect(self.on_start_camera)
        self.__table.setCellWidget(position, 7, row_item.button_stop)
        row_item.signal_stop.connect(self.on_stop_camera)
        self.__table.setCellWidget(position, 8, row_item.button_edit_info)
        row_item.signal_edit.connect(self.on_edit_camera)
        self.__table.resizeColumnsToContents()
    
    def find_row_by_camera(self, camera:Camera):
        if self.__list_row_items:
            for e in self.__list_row_items:
                if e.camera == camera:
                    return e
        return None       
    
    def handle_update_new_camera_info(self):
        for e in self.__list_row_items:
            e.reupdate_new_camera_info()
 
    def update_table(self):
        if self.__list_row_items:
            for i, v in enumerate(self.__list_row_items):
                self.insert_row_to_position(i, v)
    
    def convert_list_camera_to_row_item(self):
        for e in self.__list_camera:
            row_item = RowItem(e)
            row_item.set_state_link("ON") if e.old_state else row_item.set_state_link("OFF")
            self.__list_row_items.append(row_item)
    
    def on_start_camera(self, camera):
        self.signal_start_camera.emit(camera)
        
    def on_stop_camera(self, camera):
        self.signal_stop_camera.emit(camera)
     
    def on_edit_camera(self, camera):
        self.signal_edit_camera.emit(camera)
           
    # def disable_start_button(self, camera):
    #     self.find_row_by_camera(camera).disable_start_button()
        
    # def disable_stop_button(self, camera):
    #     self.find_row_by_camera(camera).disable_stop_button()
    
    def set_state_link(self, camera: Camera, state_str):
        row = self.find_row_by_camera(camera)
        row.set_state_link(state_str)