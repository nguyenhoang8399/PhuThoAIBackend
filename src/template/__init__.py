# from .main_window import Ui_MainWindow
from .main_window_premium import Ui_MainWindow
from .wg_camera import Ui_WgCamera
from .wg_log import Ui_WgLog
from .wg_setting import Ui_WgSetting
from .wg_draw_polygon import Ui_DrawPolygon
from .wg_camera_manager import Ui_CameraManager
from .wg_license import Ui_WgLicense
from .wg_data_manager import Ui_WgDataManager