# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file './resources/uis/new_main_window.ui'
#
# Created by: PyQt5 UI code generator 5.15.7
#
# WARNING: Any manual changes made to this file will be lost when pyuic5 is
# run again.  Do not edit this file unless you know what you are doing.


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(1280, 788)
        MainWindow.setStyleSheet("#MainWindow{\n"
"background: #dadad2;\n"
"}")
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setMinimumSize(QtCore.QSize(1280, 720))
        self.centralwidget.setObjectName("centralwidget")
        self.gridLayout_3 = QtWidgets.QGridLayout(self.centralwidget)
        self.gridLayout_3.setObjectName("gridLayout_3")
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setMinimumSize(QtCore.QSize(0, 30))
        self.label.setMaximumSize(QtCore.QSize(16777215, 30))
        self.label.setStyleSheet("margin-left: 10px;")
        self.label.setObjectName("label")
        self.gridLayout_3.addWidget(self.label, 2, 0, 1, 1)
        self.frame = QtWidgets.QFrame(self.centralwidget)
        self.frame.setMaximumSize(QtCore.QSize(16777215, 40))
        self.frame.setStyleSheet("QFrame{\n"
"background: white;\n"
"border-bottom: 1px dash;\n"
"border-color: black;\n"
"border-radius: 5px;\n"
"}\n"
"\n"
"")
        self.frame.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame.setObjectName("frame")
        self.gridLayout_2 = QtWidgets.QGridLayout(self.frame)
        self.gridLayout_2.setContentsMargins(10, 0, 10, 0)
        self.gridLayout_2.setHorizontalSpacing(3)
        self.gridLayout_2.setVerticalSpacing(0)
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.btn_veh_monitor = QtWidgets.QPushButton(self.frame)
        self.btn_veh_monitor.setMinimumSize(QtCore.QSize(200, 30))
        self.btn_veh_monitor.setMaximumSize(QtCore.QSize(150, 30))
        self.btn_veh_monitor.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.btn_veh_monitor.setToolTip("")
        self.btn_veh_monitor.setToolTipDuration(3)
        self.btn_veh_monitor.setStyleSheet("QPushButton{\n"
"color: #666666;\n"
"background: #ebebcc;\n"
"font-weight: bold;\n"
"border-radius: 10px;\n"
"}\n"
"QPushButton:hover{\n"
"background: #91917d;\n"
"color: white;\n"
"}")
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(":/icons/resources/icons/car.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.btn_veh_monitor.setIcon(icon)
        self.btn_veh_monitor.setIconSize(QtCore.QSize(20, 20))
        self.btn_veh_monitor.setObjectName("btn_veh_monitor")
        self.gridLayout_2.addWidget(self.btn_veh_monitor, 0, 2, 1, 1)
        self.btn_data_manager = QtWidgets.QPushButton(self.frame)
        self.btn_data_manager.setMinimumSize(QtCore.QSize(200, 30))
        self.btn_data_manager.setMaximumSize(QtCore.QSize(150, 30))
        self.btn_data_manager.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.btn_data_manager.setToolTip("")
        self.btn_data_manager.setToolTipDuration(3)
        self.btn_data_manager.setStyleSheet("QPushButton{\n"
"color: #666666;\n"
"background: #ebebcc;\n"
"font-weight: bold;\n"
"border-radius: 10px;\n"
"}\n"
"QPushButton:hover{\n"
"background: #91917d;\n"
"color: white;\n"
"}")
        icon1 = QtGui.QIcon()
        icon1.addPixmap(QtGui.QPixmap(":/icons/resources/icons/update.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.btn_data_manager.setIcon(icon1)
        self.btn_data_manager.setIconSize(QtCore.QSize(20, 20))
        self.btn_data_manager.setObjectName("btn_data_manager")
        self.gridLayout_2.addWidget(self.btn_data_manager, 0, 6, 1, 1)
        self.btn_camera_manager = QtWidgets.QPushButton(self.frame)
        self.btn_camera_manager.setMinimumSize(QtCore.QSize(200, 30))
        self.btn_camera_manager.setMaximumSize(QtCore.QSize(150, 30))
        self.btn_camera_manager.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.btn_camera_manager.setToolTip("")
        self.btn_camera_manager.setToolTipDuration(3)
        self.btn_camera_manager.setStyleSheet("QPushButton{\n"
"color: #666666;\n"
"background: #ebebcc;\n"
"font-weight: bold;\n"
"border-radius: 10px;\n"
"}\n"
"QPushButton:hover{\n"
"background: #91917d;\n"
"color: white;\n"
"}")
        icon2 = QtGui.QIcon()
        icon2.addPixmap(QtGui.QPixmap(":/icons/resources/icons/camera.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.btn_camera_manager.setIcon(icon2)
        self.btn_camera_manager.setIconSize(QtCore.QSize(20, 20))
        self.btn_camera_manager.setObjectName("btn_camera_manager")
        self.gridLayout_2.addWidget(self.btn_camera_manager, 0, 1, 1, 1)
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout_2.addItem(spacerItem, 0, 8, 1, 1)
        self.btn_face_monitor = QtWidgets.QPushButton(self.frame)
        self.btn_face_monitor.setMinimumSize(QtCore.QSize(200, 30))
        self.btn_face_monitor.setMaximumSize(QtCore.QSize(150, 30))
        self.btn_face_monitor.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.btn_face_monitor.setToolTip("")
        self.btn_face_monitor.setToolTipDuration(3)
        self.btn_face_monitor.setStyleSheet("QPushButton{\n"
"color: #666666;\n"
"background: #ebebcc;\n"
"font-weight: bold;\n"
"border-radius: 10px;\n"
"}\n"
"QPushButton:hover{\n"
"background: #91917d;\n"
"color: white;\n"
"}")
        icon3 = QtGui.QIcon()
        icon3.addPixmap(QtGui.QPixmap(":/icons/resources/icons/face.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.btn_face_monitor.setIcon(icon3)
        self.btn_face_monitor.setIconSize(QtCore.QSize(20, 20))
        self.btn_face_monitor.setObjectName("btn_face_monitor")
        self.gridLayout_2.addWidget(self.btn_face_monitor, 0, 3, 1, 1)
        self.btn_restart_app = QtWidgets.QPushButton(self.frame)
        self.btn_restart_app.setMinimumSize(QtCore.QSize(200, 30))
        self.btn_restart_app.setMaximumSize(QtCore.QSize(150, 30))
        self.btn_restart_app.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.btn_restart_app.setToolTip("")
        self.btn_restart_app.setToolTipDuration(3)
        self.btn_restart_app.setStyleSheet("QPushButton{\n"
"color: #666666;\n"
"background: #ebebcc;\n"
"font-weight: bold;\n"
"border-radius: 10px;\n"
"}\n"
"QPushButton:hover{\n"
"background: #91917d;\n"
"color: white;\n"
"}")
        icon4 = QtGui.QIcon()
        icon4.addPixmap(QtGui.QPixmap(":/icons/resources/icons/power.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.btn_restart_app.setIcon(icon4)
        self.btn_restart_app.setIconSize(QtCore.QSize(20, 20))
        self.btn_restart_app.setObjectName("btn_restart_app")
        self.gridLayout_2.addWidget(self.btn_restart_app, 0, 7, 1, 1)
        self.gridLayout_3.addWidget(self.frame, 0, 0, 1, 1)
        self.qframe_camera = QtWidgets.QFrame(self.centralwidget)
        self.qframe_camera.setMinimumSize(QtCore.QSize(0, 600))
        self.qframe_camera.setStyleSheet("")
        self.qframe_camera.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.qframe_camera.setFrameShadow(QtWidgets.QFrame.Raised)
        self.qframe_camera.setObjectName("qframe_camera")
        self.gridLayout_3.addWidget(self.qframe_camera, 1, 0, 1, 1)
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 1280, 26))
        self.menubar.setObjectName("menubar")
        self.menuTools = QtWidgets.QMenu(self.menubar)
        self.menuTools.setObjectName("menuTools")
        self.menuHelp = QtWidgets.QMenu(self.menubar)
        self.menuHelp.setObjectName("menuHelp")
        self.menuT_p_tin = QtWidgets.QMenu(self.menubar)
        self.menuT_p_tin.setObjectName("menuT_p_tin")
        self.setup_menu = QtWidgets.QMenu(self.menubar)
        self.setup_menu.setObjectName("setup_menu")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)
        self.actionDanh_s_ch_thi_t_b = QtWidgets.QAction(MainWindow)
        self.actionDanh_s_ch_thi_t_b.setObjectName("actionDanh_s_ch_thi_t_b")
        self.actionC_i_t_th_c_ng = QtWidgets.QAction(MainWindow)
        self.actionC_i_t_th_c_ng.setObjectName("actionC_i_t_th_c_ng")
        self.actionKi_m_tra_l_ch_s = QtWidgets.QAction(MainWindow)
        self.actionKi_m_tra_l_ch_s.setObjectName("actionKi_m_tra_l_ch_s")
        self.actionH_ng_d_n_s_d_ng = QtWidgets.QAction(MainWindow)
        self.actionH_ng_d_n_s_d_ng.setObjectName("actionH_ng_d_n_s_d_ng")
        self.actionConsole = QtWidgets.QAction(MainWindow)
        self.actionConsole.setObjectName("actionConsole")
        self.actionConsole_2 = QtWidgets.QAction(MainWindow)
        self.actionConsole_2.setObjectName("actionConsole_2")
        self.actionKh_i_ng_l_i = QtWidgets.QAction(MainWindow)
        self.actionKh_i_ng_l_i.setObjectName("actionKh_i_ng_l_i")
        self.actionTheo_d_i_ph_ng_ti_n = QtWidgets.QAction(MainWindow)
        self.actionTheo_d_i_ph_ng_ti_n.setObjectName("actionTheo_d_i_ph_ng_ti_n")
        self.actionTheo_d_i_i_t_ng = QtWidgets.QAction(MainWindow)
        self.actionTheo_d_i_i_t_ng.setObjectName("actionTheo_d_i_i_t_ng")
        self.action_video_setup = QtWidgets.QAction(MainWindow)
        self.action_video_setup.setObjectName("action_video_setup")
        self.actionTh_ng_tin_phi_n_b_n = QtWidgets.QAction(MainWindow)
        self.actionTh_ng_tin_phi_n_b_n.setObjectName("actionTh_ng_tin_phi_n_b_n")
        self.action_polygon_setup = QtWidgets.QAction(MainWindow)
        self.action_polygon_setup.setObjectName("action_polygon_setup")
        self.menuTools.addAction(self.actionDanh_s_ch_thi_t_b)
        self.menuTools.addAction(self.actionConsole_2)
        self.menuTools.addAction(self.actionTheo_d_i_ph_ng_ti_n)
        self.menuTools.addAction(self.actionTheo_d_i_i_t_ng)
        self.menuHelp.addAction(self.actionH_ng_d_n_s_d_ng)
        self.menuHelp.addAction(self.actionTh_ng_tin_phi_n_b_n)
        self.menuT_p_tin.addAction(self.actionKh_i_ng_l_i)
        self.setup_menu.addAction(self.action_video_setup)
        self.setup_menu.addAction(self.action_polygon_setup)
        self.menubar.addAction(self.menuT_p_tin.menuAction())
        self.menubar.addAction(self.menuTools.menuAction())
        self.menubar.addAction(self.setup_menu.menuAction())
        self.menubar.addAction(self.menuHelp.menuAction())

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.label.setText(_translate("MainWindow", "© 2023 Copyright by VNPT"))
        self.btn_veh_monitor.setText(_translate("MainWindow", "Giám sát phương tiện"))
        self.btn_data_manager.setText(_translate("MainWindow", "Quản lý dữ liệu"))
        self.btn_camera_manager.setText(_translate("MainWindow", "Quản lý camera"))
        self.btn_face_monitor.setText(_translate("MainWindow", "Giám sát đối tượng"))
        self.btn_restart_app.setText(_translate("MainWindow", "Khởi động lại"))
        self.menuTools.setTitle(_translate("MainWindow", "Công cụ"))
        self.menuHelp.setTitle(_translate("MainWindow", "Trợ giúp"))
        self.menuT_p_tin.setTitle(_translate("MainWindow", "Tệp tin"))
        self.setup_menu.setTitle(_translate("MainWindow", "Cài đặt"))
        self.actionDanh_s_ch_thi_t_b.setText(_translate("MainWindow", "Quản lý thiết bị"))
        self.actionC_i_t_th_c_ng.setText(_translate("MainWindow", "Cài đặt thủ công"))
        self.actionKi_m_tra_l_ch_s.setText(_translate("MainWindow", "Kiểm tra lịch sử"))
        self.actionH_ng_d_n_s_d_ng.setText(_translate("MainWindow", "Hướng dẫn sử dụng"))
        self.actionConsole.setText(_translate("MainWindow", "Console"))
        self.actionConsole_2.setText(_translate("MainWindow", "Thống kê dữ liệu"))
        self.actionKh_i_ng_l_i.setText(_translate("MainWindow", "Khởi động lại"))
        self.actionTheo_d_i_ph_ng_ti_n.setText(_translate("MainWindow", "Theo dõi phương tiện"))
        self.actionTheo_d_i_i_t_ng.setText(_translate("MainWindow", "Theo dõi đối tượng"))
        self.action_video_setup.setText(_translate("MainWindow", "Cấu hình video"))
        self.actionTh_ng_tin_phi_n_b_n.setText(_translate("MainWindow", "Thông tin phiên bản phần mềm"))
        self.action_polygon_setup.setText(_translate("MainWindow", "Thiết lập vùng giám sát"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())
