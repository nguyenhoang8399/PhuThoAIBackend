# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file './resources/uis/wg_settings.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_WgSetting(object):
    def setupUi(self, WgSetting):
        WgSetting.setObjectName("WgSetting")
        WgSetting.resize(659, 303)
        self.gridLayout = QtWidgets.QGridLayout(WgSetting)
        self.gridLayout.setObjectName("gridLayout")
        self.horizontalLayout_3 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_3.setObjectName("horizontalLayout_3")
        self.label_3 = QtWidgets.QLabel(WgSetting)
        self.label_3.setObjectName("label_3")
        self.horizontalLayout_3.addWidget(self.label_3)
        self.qline_camera_name = QtWidgets.QLineEdit(WgSetting)
        self.qline_camera_name.setObjectName("qline_camera_name")
        self.horizontalLayout_3.addWidget(self.qline_camera_name)
        self.gridLayout.addLayout(self.horizontalLayout_3, 0, 0, 1, 1)
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.label_2 = QtWidgets.QLabel(WgSetting)
        self.label_2.setObjectName("label_2")
        self.horizontalLayout_2.addWidget(self.label_2)
        self.qcb_function = QtWidgets.QComboBox(WgSetting)
        self.qcb_function.setObjectName("qcb_function")
        self.horizontalLayout_2.addWidget(self.qcb_function)
        self.gridLayout.addLayout(self.horizontalLayout_2, 1, 0, 1, 1)
        self.horizontalLayout_5 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_5.setObjectName("horizontalLayout_5")
        self.label_4 = QtWidgets.QLabel(WgSetting)
        self.label_4.setObjectName("label_4")
        self.horizontalLayout_5.addWidget(self.label_4)
        self.qline_link_camera = QtWidgets.QLineEdit(WgSetting)
        self.qline_link_camera.setObjectName("qline_link_camera")
        self.horizontalLayout_5.addWidget(self.qline_link_camera)
        self.gridLayout.addLayout(self.horizontalLayout_5, 2, 0, 1, 1)
        self.horizontalLayout_6 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_6.setObjectName("horizontalLayout_6")
        self.label_5 = QtWidgets.QLabel(WgSetting)
        self.label_5.setObjectName("label_5")
        self.horizontalLayout_6.addWidget(self.label_5)
        self.qline_threshold = QtWidgets.QLineEdit(WgSetting)
        self.qline_threshold.setObjectName("qline_threshold")
        self.horizontalLayout_6.addWidget(self.qline_threshold)
        self.gridLayout.addLayout(self.horizontalLayout_6, 3, 0, 1, 1)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.btn_draw_polygon = QtWidgets.QPushButton(WgSetting)
        self.btn_draw_polygon.setMinimumSize(QtCore.QSize(200, 0))
        self.btn_draw_polygon.setMaximumSize(QtCore.QSize(1111111, 16777215))
        self.btn_draw_polygon.setObjectName("btn_draw_polygon")
        self.horizontalLayout.addWidget(self.btn_draw_polygon)
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem)
        self.gridLayout.addLayout(self.horizontalLayout, 4, 0, 1, 1)
        self.horizontalLayout_4 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_4.setObjectName("horizontalLayout_4")
        self.btn_save = QtWidgets.QPushButton(WgSetting)
        self.btn_save.setObjectName("btn_save")
        self.horizontalLayout_4.addWidget(self.btn_save)
        self.btn_start = QtWidgets.QPushButton(WgSetting)
        self.btn_start.setObjectName("btn_start")
        self.horizontalLayout_4.addWidget(self.btn_start)
        self.btn_cancel = QtWidgets.QPushButton(WgSetting)
        self.btn_cancel.setObjectName("btn_cancel")
        self.horizontalLayout_4.addWidget(self.btn_cancel)
        self.gridLayout.addLayout(self.horizontalLayout_4, 5, 0, 1, 1)

        self.retranslateUi(WgSetting)
        QtCore.QMetaObject.connectSlotsByName(WgSetting)

    def retranslateUi(self, WgSetting):
        _translate = QtCore.QCoreApplication.translate
        WgSetting.setWindowTitle(_translate("WgSetting", "Cài đặt Camera"))
        self.label_3.setText(_translate("WgSetting", "Tên camera"))
        self.label_2.setText(_translate("WgSetting", "Chức năng"))
        self.label_4.setText(_translate("WgSetting", "Đường dẫn"))
        self.label_5.setText(_translate("WgSetting", "Mức ngưỡng"))
        self.btn_draw_polygon.setText(_translate("WgSetting", "Vẽ vùng nhận diện"))
        self.btn_save.setText(_translate("WgSetting", "Lưu"))
        self.btn_start.setText(_translate("WgSetting", "Bắt đầu"))
        self.btn_cancel.setText(_translate("WgSetting", "Hủy"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    WgSetting = QtWidgets.QWidget()
    ui = Ui_WgSetting()
    ui.setupUi(WgSetting)
    WgSetting.show()
    sys.exit(app.exec_())

