# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file './resources/uis/wg_camera.ui'
#
# Created by: PyQt5 UI code generator 5.15.7
#
# WARNING: Any manual changes made to this file will be lost when pyuic5 is
# run again.  Do not edit this file unless you know what you are doing.


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_WgCamera(object):
    def setupUi(self, WgCamera):
        WgCamera.setObjectName("WgCamera")
        WgCamera.resize(876, 663)
        self.gridLayout = QtWidgets.QGridLayout(WgCamera)
        self.gridLayout.setContentsMargins(0, 0, 0, 0)
        self.gridLayout.setSpacing(0)
        self.gridLayout.setObjectName("gridLayout")
        self.qlabel_frame = QtWidgets.QLabel(WgCamera)
        self.qlabel_frame.setToolTip("")
        self.qlabel_frame.setStyleSheet("background: black\n")
        self.qlabel_frame.setText("")
        self.qlabel_frame.setObjectName("qlabel_frame")
        self.gridLayout.addWidget(self.qlabel_frame, 0, 0, 1, 1)
        self.qlabel_frame.setSizePolicy(QtWidgets.QSizePolicy.Policy.Ignored, QtWidgets.QSizePolicy.Policy.Ignored)
        self.retranslateUi(WgCamera)
        QtCore.QMetaObject.connectSlotsByName(WgCamera)

    def retranslateUi(self, WgCamera):
        _translate = QtCore.QCoreApplication.translate
        WgCamera.setWindowTitle(_translate("WgCamera", "Form"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    WgCamera = QtWidgets.QWidget()
    ui = Ui_WgCamera()
    ui.setupUi(WgCamera)
    WgCamera.show()
    sys.exit(app.exec_())
