import requests
from ..config import WEB_HOST, URL_API_GET_TOKEN
import json
import logging
logging.basicConfig(level=logging.DEBUG)

class BaseApi(object):    
    def __init__(self):
        super().__init__()
        self._token = None
        
    def get_access_token(self):
        try:
            dict_data = {
                            "client_id": "IAC_Cloud",
                            "client_secret": "1a82f1d60ba6353bb64a8fb4b05e4bc4",
                            "grant_type": "password",
                            "username": "admin",
                            "password": "Ab@123456"
                        }
            r = requests.post(URL_API_GET_TOKEN, json=dict_data)
            r = r.json()
            self._token = r['access_token']
            return True
        except Exception as e:
            return False
           
    