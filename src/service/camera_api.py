import requests
from src.config import WEB_HOST, CONST_COMP_ID
import json
import logging
from .base_api import BaseApi
logging.basicConfig(level=logging.DEBUG)
import aiohttp
import platform

SERVER_NAME = platform.node()

class CameraApi(BaseApi):
    _URL_GET_LIST_DEVICE = f"{WEB_HOST}/api/v1/camera?page=1&size=99"
    _URL_UPDATE_DEVICE = f"{WEB_HOST}/Service/api/device"
    _URL_UPDATE_STATUS_DEVICE = f"{WEB_HOST}/Service/api/device/status"
    
    def __init__(self):
        super().__init__()


    def find_all_camera(self):
        try:
            data = requests.get(self._URL_GET_LIST_DEVICE,
                                headers={"Content-Type": "application/json", 
                                         "Authorization": f"Bearer {self._token}"}, timeout=5)
            data_str =  json.dumps(data.json(), indent=4, ensure_ascii=False)
            return json.loads(data_str)
        except Exception as e:
            print(e)
            return {}

    def put_status_camera(self, camera_id, status=2):
        url = self._URL_UPDATE_STATUS_DEVICE + "/{}/{}".format(camera_id, status)
        print(url)
        try: 
            resp = requests.put(url, 
                                headers={"Content-Type": "application/json", 
                                        "Authorization": f"Bearer {self._token}"})
            return resp.json()
        except Exception as er:
            print(er)
            return {}
        
    async def put_status_camera_async(self, session:aiohttp.ClientSession, id_camera, status=2):
        url = self._URL_UPDATE_STATUS_DEVICE + "/{}/{}".format(id_camera, status)
        print(url)
        async with session.put(url, headers={"Content-Type": "application/json", 
                                                "Authorization": f"Bearer {self._token}"}) as resp:
            return await resp.json()

if __name__ == "__main__":
    api = CameraApi()
    api.get_access_token()
    payload = {'id': 71, 
               'idComp': 1, 
               'name': 'Camera giám sát phương tiện lưu thông qua cầu Văn Phú', 
               'code': 'camptcvp1021', 'link': 'rtsp://27.72.98.49:50104/profile/3',
               'status': 0, 
               'areaId': 89, 'function': 2, 'isDelete': False}
    a = api.put_status_camera(payload)
    print(a)
    