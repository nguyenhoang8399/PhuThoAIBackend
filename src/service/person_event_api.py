from .base_api import BaseApi
from ..config import WEB_HOST
import requests
from PyQt5.QtCore import QUuid
from requests.exceptions import ConnectTimeout

class PersonEventApi(BaseApi):
    _URI_PERSON_EVENT = f"{WEB_HOST}/Service/api/synchronize/personEvents"

    def __init__(self):
        super().__init__()
        
    def post_person_event(self, json_data):
        try:
            resp_person = requests.post(self._URI_PERSON_EVENT,
                                 headers={"Content-Type": "application/json",
                                          "Authorization": f"Bearer {self._token}"},
                                 json=json_data, timeout=1)

            if resp_person.status_code == 200:
                return resp_person.json()
            elif resp_person.status_code == 400:
                return {}
            elif resp_person.status_code == 500:
                return {}
            else:
                self.get_access_token()
                resp_person = requests.post(self._URI_PERSON_EVENT,
                              headers={"Content-Type": "application/json",
                                       "Authorization": f"Bearer {self._token}"},
                              json=json_data, timeout=20)
                return resp_person.json()
        except ConnectTimeout:
            raise ConnectTimeout("Timeout when post person event")
        
        except Exception as e:
            print(e)
            return {}

if __name__ == "__main__":
    data = {
        "eventId": QUuid.createUuid().toString(QUuid.StringFormat.WithoutBraces),
        "personId": "FA695287-237B-4EDB-8815-0A551AE66F45",
        "deviceId": "67",
        "accessDate": "2022-10-31 17:30:00",
        "status": "1",
        "image": "/media/torage/people/images/358AAFE6-F9A7-4367-83D8-CDB372072E48_30A99999_randomtext.jpg",
        "video": "/media/torage/people/video/358AAFE6-F9A7-4367-83D8-CDB372072E48_30A99999_random.mp4"
    }
    
    pea = PersonEventApi()
    resp = pea.post_person_event(data)
    print(resp)
