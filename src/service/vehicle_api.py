from .base_api import BaseApi
from ..config import WEB_HOST
import requests
from datetime import datetime
from PyQt5.QtCore import QUuid
from requests.exceptions import ConnectTimeout


class VehicleApi(BaseApi):
    URI_VEHICLE_EVENT = f"{WEB_HOST}/api/v1/vehicle-event"
    URL_VEHICLE_IN_BLACK_LIST = f"{WEB_HOST}/Service/api/vehicles?page=1&itemsPerPage=99999999&FilterCompId=1&FilterStatus=1"

    def __init__(self):
        super().__init__()
        # self.get_access_token()
        
    def post_veh_event(self, json_data):
        try:
            resp_veh = requests.post(self.URI_VEHICLE_EVENT,
                                 headers={"Content-Type": "application/json"},
                                 json=json_data, timeout=2)
            return resp_veh.json()
            
        except ConnectTimeout:
            raise ConnectTimeout("Timeout when post vehicle event")
        except Exception as e:
            print(e)
            print("Error when post vehicle event ******")
            print("***: ", json_data)
            return {}
        
    def find_all_vehicle_in_black_list(self):
        try:
            resp = requests.get(self.URL_VEHICLE_IN_BLACK_LIST,
                        headers={"Content-Type": "application/json",
                                "Authorization": f"Bearer {self._token}"})
            return list(map(lambda x: x["licensePlates"],resp.json()['data']))
        except Exception as e:
            print(e)
            print("Error when get vehicle in black list ******")
            return []
        

# if __name__ == "__main__":
#     data = {
#         "eventId": QUuid.createUuid().toString(QUuid.StringFormat.WithoutBraces),
#         "vehicleId": "0",
#         "vehicleLicensePlates": "28A11111",
#         "vehicleType": "1",
#         "type": "1",
#         "deviceId": "67",
#         "accessTime": "2022-09-06 16:30:00",
#         "image": "/media/torage/people/images/358AAFE6-F9A7-4367-83D8-CDB372072E48_30A99999_randomtext.jpg",
#         "coordinates": "",
#         "video": "/media/torage/people/video/358AAFE6-F9A7-4367-83D8-CDB372072E48_30A99999_random.mp4"
#     }
#     pea = VehicleApi()
#     resp = pea.post_veh_event(data)
#     print(resp)
