import requests
from ..config import CONST_URL_RECOGNIZE_FACE
import cv2

class FaceRecogApi:
    @staticmethod
    def recognize_face(payload:dict):
        try:
            res = requests.post(url=CONST_URL_RECOGNIZE_FACE, json=payload, timeout=3)
            print("*"*20)
            print(res.elapsed.total_seconds())
            return res.json()
        except Exception as e:
            res = {"status": 500}
            print("Error when recognize face")
            print(e)
        return res
    