
from .setup import read_model_config_file
from .plate_casting import check_plate
# from .detect import Tracking, Detection
from .detect_bt import Tracking, Detection, Classify, relu, VehicleTracking
from .face_classify import FaceClassify
