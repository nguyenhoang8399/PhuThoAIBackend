from typing import List
from .polygon import Polygon
import cv2
import os
from urllib.parse import urlparse


class Camera(object):
    def __init__(self):
        super().__init__()
        self.id = None
        self.id_company = 1
        self.area_code = ""
        self.area_id = 7
        self.name = ""
        self.code = "PT"
        self.link = ""
        self.function = 1
        self.polygon:Polygon
        self.status = 0
        
        self.streaming_fps = 25
        self.record_fps = 10
        
        self.old_state = False

    def merge_data(self, __o:"Camera"):
        self.id_company    = __o.id_company
        self.area_code     = __o.area_code
        self.name          = __o.name
        self.code          = __o.code
        self.link          = __o.link
        self.function      = __o.function
        self.polygon       = __o.polygon
        self.status        = __o.status
        
    def check_difference(self, __o:"Camera"):
        is_changed_area_code = self.area_code == __o.area_code
        is_changed_code = self.code == __o.code
        is_changed_link = self.link == __o.link
        is_changed_function = self.function == __o.function
        is_change_polygon = self.polygon == __o.polygon
        return is_changed_area_code or is_changed_code \
            or is_changed_link or is_changed_function \
                or is_change_polygon
    
    @staticmethod
    def deserialize(data) -> "Camera":
        try:
            new_camera = Camera()
            new_camera.name = data["name"]
            new_camera.id = data["id"]
            new_camera.function = data["idCameraFunction"]
            new_camera.link = data["url"]
            new_camera.area_id = int(data["idArea"])
            new_camera.id_company = data["idCompany"]
            new_camera.status = data["status"]
        except Exception as e:
            print(e)
        return new_camera

    @staticmethod
    def desirialize_list_data(list_data_dict:dict) -> List["Camera"]:
        list_camera:List[Camera] = []
        for e in list_data_dict:
            list_camera.append(Camera.deserialize(e))
        return list_camera
  
    
    def is_different_from(self, __o:'Camera'):
        is_changed_ = (self.id_company != __o.id_company) \
            or (self.area_code != __o.area_code) \
                or (self.code != __o.code) \
                    or (self.link != __o.link) \
                        or (self.function != __o.function) 
                            # or self.polygon == __o.polygon
        return is_changed_
    
    def __eq__(self, __o: 'Camera') -> bool:
        return self.id == __o.id
    
    def toString(self):
        return f"{self.id} {self.link} {self.code} {self.area_code}"