from datetime import datetime
from PyQt5.QtCore import QDateTime
from .base_result import BaseResult


class PersonResult(BaseResult):
    def __init__(self):
        super().__init__()
        self.age = 1
        self.features = ""
        self.gender = 1
        self.person_id = None
        self.tracked_id = 0

    @staticmethod
    def serialize(rs:"PersonResult"):
        data = {
                "eventId": rs.event_id,
                "personId": "00000000-0000-0000-0000-000000000000",
                "features": rs.features,
                "age": rs.age,
                "gender": rs.gender,
                "deviceId": str(rs.camera_id),
                "accessDate": datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
                "image": str(rs.img_path),
                "video": str(rs.video_path)
            }
        if rs.person_id is not None:
            data["personId"] = rs.person_id
        return data
    
    @staticmethod
    def serialize_for_debug(rs:"PersonResult"):
        data = {
                "eventId": rs.event_id,
                "personId": "00000000-0000-0000-0000-000000000000",
                "age": rs.age,
                "gender": rs.gender,
                "deviceId": str(rs.camera_id),
                "accessDate": datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
                "image": str(rs.img_path),
                "video": str(rs.video_path)
            }
        if rs.person_id is not None:
            data["personId"] = rs.person_id
        return data
    
    def toString(self):
        return "eventid: {} personId: {} age: {} gender: {} accessDate: {} ".format(self.eventID, self.person_id, self.age, self.gender, self.dt)