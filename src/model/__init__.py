from .person_result import PersonResult
from .vehicle_result import VehicleResult
from .camera import Camera
from .image import Image
from .polygon import Polygon
from .base_result import BaseResult