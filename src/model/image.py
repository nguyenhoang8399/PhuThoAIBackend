import numpy as np

class Image(object):
    def __init__(self, image: np.ndarray, time_stamp):
        self.value = image
        self.time_stamp = time_stamp 