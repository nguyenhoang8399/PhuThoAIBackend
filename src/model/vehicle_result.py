from datetime import datetime
from PyQt5.QtCore import QDateTime, QUuid
from .base_result import BaseResult

# datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
class VehicleResult(BaseResult):
    def __init__(self):
        super().__init__()
        self.digit = None
        self.cls = 0
        self.type = None
        self.plate_img_path = ""
        self.is_blacklist = False

    @staticmethod
    def serialize(rs:"VehicleResult"):
        data = {
            "plateNumber": rs.digit,
            "idCamera": str(rs.camera_id),
            "idArea": str(rs.area_id),
            "createdTime": datetime.now().isoformat(),
            "overviewImagePath": str(rs.img_path),
            "videoPath": str(rs.video_path),
            "plateImagePath": str(rs.plate_img_path),
            "isBlacklist": rs.is_blacklist
        }
        return data
    
    