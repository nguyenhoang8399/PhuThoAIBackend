from datetime import datetime
from PyQt5.QtCore import QDateTime, QUuid


class BaseResult(object):
    def __init__(self):
        self.id = None
        self.dt = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        self.img_path = ''
        self.video_path = ''   
        self.camera_id = 0
        self.area_id = 0
        self.event_id =  QUuid.createUuid().toString(QUuid.StringFormat.WithoutBraces)
        self.current_dt = QDateTime.currentDateTime()
