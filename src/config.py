import os
from os.path import dirname
import yaml
import boto3

ROOT = dirname(dirname(os.path.abspath(__file__))) # pointer to outside of gsan module

CONFIG_PATH = os.path.join(ROOT, "resources", "config", "config.yaml")

def read_yaml_file(path):
    with open(path, "r") as f:
        data = yaml.safe_load(f)
    return data
CONFIG_DATA = read_yaml_file(CONFIG_PATH)

WEB_HOST_FACE_ENGINE = CONFIG_DATA["WEB_HOST_FACE_ENGINE"]
CONST_URL_RECOGNIZE_FACE = f"{WEB_HOST_FACE_ENGINE}/api/v1/face-search"
CONST_URL_FACE_ENGINE_UPDATE = f"{WEB_HOST_FACE_ENGINE}/api/v1/reload-engine"

# Runtime project config
ROOT_SAVE_FILE_PERSON = CONFIG_DATA["ROOT_SAVE_FILE_PERSON"]
ROOT_SAVE_FILE_VEHICLE = CONFIG_DATA["ROOT_SAVE_FILE_VEHICLE"]
ROOT_SAVE_FILE = CONFIG_DATA["ROOT_SAVE_FILE"]

# API config
WEB_HOST = CONFIG_DATA["WEB_HOST"]
URL_API_GET_TOKEN = f"{WEB_HOST}/Service/api/token/auth"
WEB_HOST_POLYGON = CONFIG_DATA["WEB_HOST_POLYGON"]

# media server
CONST_MEDIA_SERVER_HOST = CONFIG_DATA["CONST_MEDIA_SERVER_HOST"]
CONST_COMP_ID = CONFIG_DATA["CONST_COMP_ID"]

# STREAMING


class VideoConfig:
    _480p: tuple = (640, 480)
    _720p: tuple = (1280, 720)
    _1080p: tuple = (1920, 720)


CONST_STREAMING_SIZE = CONFIG_DATA["CONST_STREAMING_SIZE"]

DEBUG_MODE = CONFIG_DATA["DEBUG_MODE"]

HALF_MODEL = CONFIG_DATA["HALF_MODEL"]

# OVERWRITE IMAGE
# DUE TO THE LIMITATION OF DISK
PERCENTAGE_TO_WARNING = CONFIG_DATA["PERCENTAGE_TO_WARNING"]
PERCENTAGE_TO_DELETE_FOLDER = CONFIG_DATA["PERCENTAGE_TO_DELETE_FOLDER"]
NUMBER_OF_DAYS = CONFIG_DATA["NUMBER_OF_DAYS"]
HOUR_TO_RUN_CHECKING = CONFIG_DATA["HOUR_TO_RUN_CHECKING"]
MINUTE_TO_RUN_CHECKING = CONFIG_DATA["MINUTE_TO_RUN_CHECKING"]

# CONFIG CLOUD STORAGE
HOST_S3 = CONFIG_DATA["HOST_S3"]
AWS_ACCESS_KEY_ID = CONFIG_DATA["AWS_ACCESS_KEY_ID"]
AWS_SECRET_ACCESS_KEY = CONFIG_DATA["AWS_SECRET_ACCESS_KEY"]

S3_RESOURCE = boto3.resource('s3', endpoint_url=HOST_S3,
                             aws_access_key_id=AWS_ACCESS_KEY_ID,
                             aws_secret_access_key=AWS_SECRET_ACCESS_KEY)

S3_CLIENT = boto3.client('s3', endpoint_url=HOST_S3,
                         aws_access_key_id=AWS_ACCESS_KEY_ID,
                         aws_secret_access_key=AWS_SECRET_ACCESS_KEY)

S3_BUCKET_NAME = CONFIG_DATA["S3_BUCKET_NAME"]

