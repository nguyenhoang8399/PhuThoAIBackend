from matplotlib import pyplot
pyplot.set_loglevel("notset")
import sys

from os.path import dirname
CONST_FOLDER_PATH = dirname(dirname(__file__)) 
import os
os.environ["KMP_DUPLICATE_LIB_OK"] = "TRUE"
sys.path.append(CONST_FOLDER_PATH)
import logging
from PyQt5 import QtGui
from PyQt5.QtWidgets import QApplication

from src.controller import MainWindowController

logging.getLogger("requests").setLevel(logging.WARNING)
logging.getLogger("urllib3").setLevel(logging.WARNING)
logging.getLogger("getmac").setLevel(logging.WARNING)
logging.getLogger("boto3").setLevel(logging.NOTSET)
logging.getLogger("botocore").setLevel(logging.WARNING)

import signal


try:
    from ctypes import windll  # Only exists on Windows.
    myappid = 'mycompany.myproduct.subproduct.version'
    windll.shell32.SetCurrentProcessExplicitAppUserModelID(myappid)
except ImportError:
    pass

# def find_all_app_names():
#     """Find all opened programs' names"""
#     app_names = []
#     for app in psutil.process_iter():
#         try:
#             app_names.append((app.pid, app.name()))
#         except psutil.NoSuchProcess:
#             pass
#     return app_names

# def check_opened_program():
#     name_app = "GIAM SAT AN NINH.exe"
#     all_app = find_all_app_names()
#     current_pid = os.getpid()
#     for pid, name in all_app:
#         if pid != current_pid and name == name_app:
#             return True
#     return False


if __name__ == "__main__":
    signal.signal(signal.SIGINT, signal.SIG_DFL)
    app = QApplication(sys.argv)
    main_window = MainWindowController()
    main_window.show()
    
    # license_dialog = WgLicenseController()
    # is_activated = license_dialog.check_license_file()
    # range_ = 3
    
    # if is_activated:
    #     main_window = MainWindowController()
    #     main_window.show()
    # else:
    #     rs = license_dialog.exec_()
    #     for i in range(3):
    #         if i == range_ :
    #             sys.exit()
    #         if rs == QDialog.Accepted:
    #             if license_dialog.is_valid:
    #                 main_window = MainWindowController()
    #                 main_window.show()
    #                 break
    #             else:
    #                 rs = license_dialog.exec_()
    #         else: 
    #             break
           

    sys.exit(app.exec_())

